FROM rust:slim-buster

ENV CLOUD_HOME=/home/emcloud

RUN apt-get update \
  && apt-get install -y libpq-dev make libssl-dev pkg-config

# Set up timezone
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime \
  && echo "Europe/Paris" > /etc/timezone

RUN groupadd -r -g 1001 emcloud && useradd -mr -g emcloud -d ${CLOUD_HOME} -s /bin/bash -u 1000 emcloud

USER emcloud
WORKDIR ${CLOUD_HOME}
RUN rustup toolchain install stable

COPY --chown=emcloud:emcloud . ${CLOUD_HOME}
RUN cargo build --release

EXPOSE 3000

VOLUME [ "${CARGO_HOME}" ]
VOLUME [ "${CLOUD_HOME}/target/release" ]

CMD [ "target/release/emcloud" ]
