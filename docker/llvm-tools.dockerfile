FROM rustlang/rust:nightly

RUN apt-get update && apt-get -y install bc
RUN rustup component add llvm-tools-preview
RUN cargo install grcov
