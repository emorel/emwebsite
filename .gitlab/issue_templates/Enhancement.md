# Problem to solve

(Explain the problem this new feature is supposed to resolve)

# Technical details

(Add any details, technical challenges, or other considerations for correct
implementation here)

# Usage examples

(Add one or more code snippet examples on how this feature would be used,
preferably in an easy-to-transcribe test case)

# External Resources

(OPTIONAL: any external resources, such as blog posts, documentations,
library examples or others, that may help solving the problem)

/label ~Enhancement
