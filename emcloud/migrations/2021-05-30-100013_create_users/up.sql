CREATE TABLE users (
    id SMALLINT PRIMARY KEY NOT NULL UNIQUE CHECK (id = 1),
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    summary VARCHAR(100),
    description TEXT
);

COMMENT ON TABLE users IS 'A single user exists for the system, who is the administrator.';
COMMENT ON COLUMN users.id IS 'A primary key. The constraint on the column ensures only 1 row may exist at a time, where id=1.';
COMMENT ON COLUMN users.email IS 'A valid email address used, in part, to authenticate and authorize a user in conjunction with their password.';
COMMENT ON COLUMN users.password IS 'An encrypted password used to authenticate and authorize a user in conjunction with their email address.';
COMMENT ON COLUMN users.first_name IS 'The real-life first name of the user.';
COMMENT ON COLUMN users.last_name IS 'The real-life last name of the user.';
COMMENT ON COLUMN users.summary IS 'A very short description written by the user, a maximum of 100 characters is allowed.';
COMMENT ON COLUMN users.description IS 'A longer description written by the user, can be of any length.';
