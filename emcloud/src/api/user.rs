#![allow(clippy::unused_async)]

use crate::config;
use crate::models;
use crate::services::auth;
use crate::services::password;

use actix_web::http;
use actix_web::web;
use actix_web::HttpResponse;

use em_jsonapi::actix::JsonApiResponse;
use em_jsonapi::document;
use em_jsonapi::document::{IntoPrimaryData, Resource};
use em_jsonapi::Query;

pub fn routes(cfg: &mut web::ServiceConfig) {
    cfg.service(show);
    cfg.service(patch_password);
    cfg.service(log_in);
    cfg.service(log_out);
    cfg.service(verify_session);
}

#[derive(serde::Deserialize)]
struct LoginInfo {
    email: String,
    password: String,
}

#[derive(serde::Deserialize)]
#[serde(rename_all = "camelCase")]
struct PasswordPatchSet {
    password: String,
    password_confirmation: String,
}

/// Returns the user for the current session.
///
/// # Errors
///
/// * 401 when unauthenticated
#[actix_web::get("/user")]
async fn show(
    query: Query,
    session: auth::Authenticated,
) -> crate::Result<JsonApiResponse<Resource>> {
    Ok(JsonApiResponse::new().body(session.user.to_document(Some(&query))))
}

/// Updates the current user's password.
///
/// # Errors
///
/// * 401 when authorization fails
/// * 400 when passwords don't match
#[actix_web::patch("/user/password")]
async fn patch_password(
    patch_set: web::Json<PasswordPatchSet>,
    session: auth::Authenticated,
    config: web::Data<config::App>,
    pool: web::Data<config::PgPool>,
) -> crate::Result<HttpResponse> {
    if patch_set.password != patch_set.password_confirmation {
        bail!(http::StatusCode::BAD_REQUEST, "Passwords don't match")
    }

    let current_user = session.user;
    let conn = pool.get()?;

    let hashed_password = password::hash(
        &patch_set.password,
        config.secret_key.as_bytes(),
        config.argon_salt.as_bytes(),
    )?;

    web::block(move || {
        use config::schema::users::dsl::password;
        use diesel::prelude::*;
        diesel::update(&current_user)
            .set(password.eq(hashed_password))
            .execute(&conn)
    })
    .await?;

    Ok(HttpResponse::NoContent().finish())
}

/// Authenticate the user using an email and password.
///
/// # Errors
///
/// * 400 when the password and its confirmation don't match
/// * 401 when the user is not found or the password is invalid
#[actix_web::post("/user/auth")]
async fn log_in(
    login_info: web::Json<LoginInfo>,
    session: actix_session::Session,
    config: web::Data<config::App>,
    pool: web::Data<config::PgPool>,
) -> crate::Result<JsonApiResponse<Resource>> {
    let conn = pool.get()?;
    // cannot move a struct field without moving the whole struct
    // could use the std::mem::replace technique but that seems overkill
    let email = login_info.email.clone();

    let user = web::block(move || models::User::find_by_email(&email, &conn)).await?;

    password::verify(
        &login_info.password,
        &user.password,
        config.secret_key.as_bytes(),
    )?;

    session.set("user_id", user.id)?;

    let document = document::Builder::new()
        .meta("expiry", config::COOKIE_EXPIRY)
        .link("related", "/user")
        .build();

    Ok(JsonApiResponse::new().body(document))
}

/// Signs out the current user.
///
/// This endpoint does not check if the user is actually authenticated or not,
/// and purges the session systematically.
#[actix_web::delete("/user/auth")]
async fn log_out(session: actix_session::Session) -> crate::Result<JsonApiResponse<Resource>> {
    session.purge();
    Ok(JsonApiResponse::new())
}

/// Verifies the session if any, and prolongs the session cookie.
///
/// # Errors
///
/// * 401 when the session is invalid
#[actix_web::patch("/user/auth")]
async fn verify_session(session: auth::Authenticated) -> crate::Result<JsonApiResponse<Resource>> {
    session.renew();

    let document = document::Builder::new()
        .meta("expiry", config::COOKIE_EXPIRY)
        .link("related", "/user")
        .build();

    Ok(JsonApiResponse::new().body(document))
}

#[cfg(test)]
mod tests {
    use crate::test;
    use actix_web::test as aw_test;

    #[actix_rt::test]
    async fn get_show_is_ok() {
        let (mut app, db) = test::init_app().await;
        let user = test::create_user(&db).await;
        let cookie = test::auth_cookie(&user);

        let request = test::get("/user").cookie(cookie).to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 200);
    }

    #[actix_rt::test]
    async fn post_log_in_with_invalid_password() {
        let (mut app, db) = test::init_app().await;
        test::create_user(&db).await;
        let payload = json! {
            "email": "test@mctesty.org",
            "password": "no good",
            "passwordConfirmation": "no good",
        };

        let request = test::post("/user/auth").set_json(&payload).to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 403);
    }

    #[actix_rt::test]
    async fn post_log_in_with_email_not_found() {
        let (mut app, db) = test::init_app().await;
        test::create_user(&db).await;
        let payload = json! {
            "email": "imposter@mctesty.org",
            "password": "password",
            "passwordConfirmation": "password",
        };

        let request = test::post("/user/auth").set_json(&payload).to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 404);
    }

    #[actix_rt::test]
    async fn post_log_in_is_ok() {
        let (mut app, db) = test::init_app().await;
        test::create_user(&db).await;
        let payload = json! {
            "email": "test@mctesty.org",
            "password": "password",
            "passwordConfirmation": "password",
        };

        let request = test::post("/user/auth").set_json(&payload).to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 201);
    }

    #[actix_rt::test]
    async fn delete_log_out_is_ok() {
        let (mut app, db) = test::init_app().await;
        let user = test::create_user(&db).await;
        let cookie = test::auth_cookie(&user);

        let request = test::delete("/user/auth").cookie(cookie).to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 204);

        let cookie = response
            .response()
            .cookies()
            .find(|c| c.name() == crate::config::COOKIE_NAME)
            .unwrap();

        assert_eq!(cookie.max_age().unwrap(), time::Duration::seconds(0));
        assert_eq!(
            cookie.expires().unwrap().year(),
            time::OffsetDateTime::now_utc().year() - 1
        );
    }

    #[actix_rt::test]
    async fn patch_verify_session_is_ok() {
        let (mut app, db) = test::init_app().await;
        let user = test::create_user(&db).await;
        let cookie = test::auth_cookie(&user);
        let initial_expiry = cookie.expires().unwrap();

        let request = test::patch("/user/auth").cookie(cookie).to_request();

        // need to sleep 1 second to have a difference during the assert
        std::thread::sleep(std::time::Duration::new(1, 0));

        let response = aw_test::call_service(&mut app, request).await;

        let response_cookie = response
            .response()
            .cookies()
            .find(|c| c.name() == crate::config::COOKIE_NAME)
            .unwrap();

        assert_eq!(response.status(), 200);
        assert!(response_cookie.expires().unwrap() > initial_expiry);
    }

    #[actix_rt::test]
    async fn patch_verify_session_is_unauthorized() {
        let (mut app, _db) = test::init_app().await;
        let request = test::patch("/user/auth").to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 401);
    }

    #[actix_rt::test]
    async fn patch_password_is_ok() {
        let (mut app, db) = test::init_app().await;
        let user = test::create_user(&db).await;
        let cookie = test::auth_cookie(&user);
        let payload = json! {
            "password": "hunter3",
            "passwordConfirmation": "hunter3"
        };

        let request = test::patch("/user/password")
            .cookie(cookie)
            .set_json(&payload)
            .to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 204);
    }

    #[actix_rt::test]
    async fn patch_password_is_unauthorized() {
        let (mut app, _db) = test::init_app().await;
        let payload = json! {
            "password": "hunter3",
            "passwordConfirmation": "hunter3",
        };

        let request = test::patch("/user/password")
            .set_json(&payload)
            .to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 401);
    }

    #[actix_rt::test]
    async fn patch_password_is_bad_request() {
        let (mut app, db) = test::init_app().await;
        let user = test::create_user(&db).await;
        let cookie = test::auth_cookie(&user);
        let payload = json! {
            "password": "hunter3",
            "passwordConfirmation": "hunter2"
        };

        let request = test::patch("/user/password")
            .cookie(cookie)
            .set_json(&payload)
            .to_request();
        let response = aw_test::call_service(&mut app, request).await;

        assert_eq!(response.status(), 400);
    }
}
