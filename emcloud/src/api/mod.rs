use actix_web::web;

mod user;

/// Configures the endpoint.
///
/// This function expects to be called when constructing a new [`App`].
///
/// [`App`]: actix_web::App
pub fn routes(cfg: &mut web::ServiceConfig) {
    user::routes(cfg);
}
