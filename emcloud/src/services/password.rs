use actix_web::http;
use std::fmt;

#[derive(Debug, PartialEq)]
pub enum Error {
    Argon(argon2::Error),
    Invalid,
}

impl Error {
    pub fn status_code(&self) -> http::StatusCode {
        match self {
            Self::Argon(error) => match error {
                argon2::Error::PwdTooShort => http::StatusCode::BAD_REQUEST,
                _ => http::StatusCode::INTERNAL_SERVER_ERROR,
            },
            Self::Invalid => http::StatusCode::FORBIDDEN,
        }
    }
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Argon(error) => error.fmt(f),
            Self::Invalid => f.write_str("Invalid password"),
        }
    }
}

impl From<argon2::Error> for Error {
    fn from(error: argon2::Error) -> Self {
        Self::Argon(error)
    }
}

/// Returns a hashed version of a plain password.
pub fn hash(plain: &str, secret: &[u8], salt: &[u8]) -> Result<String, Error> {
    let config = argon2::Config {
        ad: &[],
        hash_length: 32,
        lanes: 12,
        mem_cost: 65_536,
        thread_mode: argon2::ThreadMode::Parallel,
        time_cost: 4,
        variant: argon2::Variant::Argon2id,
        version: argon2::Version::Version13,
        secret,
    };

    Ok(argon2::hash_encoded(plain.as_bytes(), salt, &config)?)
}

/// Verifies the validity of a plain password against a hashed password.
pub fn verify(plain: &str, hashed: &str, secret: &[u8]) -> Result<(), Error> {
    if argon2::verify_encoded_ext(hashed, plain.as_bytes(), secret, &[])? {
        Ok(())
    } else {
        Err(Error::Invalid)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash_and_verify() {
        let plain = "hunter2";
        let secret = "do not steal !!!".as_bytes();
        let salt = "nah you can have some pepper instead".as_bytes();

        let hashed = hash(plain, secret, salt).unwrap();
        let is_valid = verify(plain, &hashed, secret);
        let is_invalid = verify("123456", &hashed, secret);

        assert!(is_valid.is_ok());
        assert_eq!(is_invalid, Err(Error::Invalid));
    }
}
