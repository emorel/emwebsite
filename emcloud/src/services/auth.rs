use crate::config;
use crate::models;

use actix_web::dev;
use actix_web::http;
use actix_web::web;
use std::future::Future;
use std::pin::Pin;

#[doc(hidden)]
fn unauthorized_error() -> crate::Error {
    crate::Error::Simple(
        "You must be signed in".to_string(),
        http::StatusCode::UNAUTHORIZED,
    )
}

/// Makes available a [`User`] and their [`Session`] that is currently
/// authenticated.
///
/// This struct implements [`FromRequest`] so it can be used as an extractor in
/// endpoints requiring authentication. It returns an error when failing to
/// verifying the session.
///
/// This implements [`Deref`] and returns the underlying [`Session`].
///
/// If you need to retrieve a session without an authenticated user, such as in
/// a handler to log in a user, use the [`Session`] extractor provided by
/// `actix_session`.
///
/// # Example
///
/// ```rust,ignore
/// #[get("/private-stuff")]
/// fn my_route(session: auth::Authenticated) -> web::Json<models::User> {
///     json!({ user: session.user })
/// }
/// ```
///
/// [`User`]: models::User
/// [`Session`]: actix_session::Session
/// [`FromRequest`]: actix_web::FromRequest
/// [`Deref`]: std::ops::Deref
pub struct Authenticated {
    pub user: models::User,
    session: actix_session::Session,
}

impl std::ops::Deref for Authenticated {
    type Target = actix_session::Session;

    fn deref(&self) -> &Self::Target {
        &self.session
    }
}

impl actix_web::FromRequest for Authenticated {
    type Error = crate::Error;

    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    type Config = ();

    fn from_request(req: &actix_web::HttpRequest, payload: &mut dev::Payload) -> Self::Future {
        let session = actix_session::Session::from_request(req, payload);
        let req = req.clone();

        Box::pin(async move {
            let session = session.await.map_err(|_| unauthorized_error())?;
            let pool = req.app_data::<web::Data<config::PgPool>>().unwrap();
            let conn = pool.get()?;

            let user_id = session
                .get::<i16>("user_id")
                .map_err(|_| unauthorized_error())?
                .ok_or_else(unauthorized_error)?;

            let user =
                models::User::find_by_id(user_id, &conn).map_err(|_| unauthorized_error())?;

            Ok(Authenticated { user, session })
        })
    }
}
