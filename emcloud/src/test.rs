//! Module used to prepare the application to run tests.
//!
//! This module offers a bunch of helper function to set up the environment to
//! run tests.
//! Notably, it allows to create a database per thread, so it can be dropped
//! safely after a test, as Cargo runs tests in parallel.
//!
//! # TODO
//!  - Look for a factory crate, or implement a factory system, to create
//!    database entities easily

#![allow(dead_code)]

use std::sync::atomic;

use crate::api;
use crate::config;
use crate::config::db;
use crate::models;
use crate::traits::FromEnv;

use diesel::prelude::*;

use actix_web::cookie;
use actix_web::dev;
use actix_web::http;
use actix_web::test;
use actix_web::web;
use actix_web::App;

use em_jsonapi::JSONAPI_CONTENT_TYPE;

static TEST_DATABASES_COUNT: atomic::AtomicI32 = atomic::AtomicI32::new(0);

/// Wrapper around a connection pool.
///
/// This struct implements [`Drop`] to delete the test database when it is
/// dropped from memory.
pub(crate) struct TestDatabase {
    name: String,
    pool: config::PgPool,
}

impl TestDatabase {
    fn create_database(name: &str) {
        let pg_conn = pg_conn();
        diesel::sql_query(format!("CREATE DATABASE {}", name))
            .execute(&pg_conn)
            .unwrap_or_else(|error| {
                panic!(
                    "failed to create test database {} due to error {:#?}",
                    name, error
                )
            });
    }

    fn create_pool(db_url: &str) -> config::PgPool {
        // create a pool with a single connection
        // given how cargo runs tests in parallel, they each receive their own pool
        let manager = db::PgDatabase::new(db_url);
        let pool = config::PgPool::builder()
            .max_size(1)
            .build(manager)
            .expect("failed to create test db pool");

        db::run_migrations(&pool).expect("Failed to run migrations");
        pool
    }

    fn new(config: &config::App) -> Self {
        let name = config.pg.db.clone();
        Self::create_database(&name);

        let pool = Self::create_pool(&config.pg.url());

        TestDatabase { name, pool }
    }

    /// Returns a reference to the pool.
    pub(crate) fn pool(&self) -> &config::PgPool {
        &self.pool
    }
}

impl Drop for TestDatabase {
    fn drop(&mut self) {
        let pg_conn = pg_conn();

        diesel::sql_query(format!("DROP DATABASE {} WITH (FORCE)", self.name))
            .execute(&pg_conn)
            .unwrap_or_else(|error| {
                panic!("dangling database {} due to error {:#?}", self.name, error)
            });
    }
}

/// Sets up the testing environment and returns a test [`App`] and a [`TestDatabase`].
///
/// [`App`]: actix_web::App
pub(crate) async fn init_app() -> (
    impl dev::Service<
        Request = actix_http::Request,
        Response = dev::ServiceResponse<impl dev::MessageBody>,
        Error = actix_web::Error,
    >,
    TestDatabase,
) {
    let config = init_app_config();
    let test_database = TestDatabase::new(&config);

    let pool = test_database.pool.clone();
    let secret = config.secret_key.as_bytes();

    let service = test::init_service(
        App::new()
            .data(pool)
            .data(config.clone())
            .wrap(config::cors())
            .wrap(config::default_headers())
            .wrap(config::auth(secret))
            .wrap(config::error_handlers())
            .configure(api::routes),
    )
    .await;

    (service, test_database)
}

/// Creates a user in the test database.
pub(crate) async fn create_user(db: &TestDatabase) -> models::User {
    let conn = db.pool.get().unwrap();
    let config = init_app_config();
    let argon_config = argon_config(config.secret_key.as_bytes());
    let hashed_password = argon2::hash_encoded(
        "password".as_bytes(),
        config.argon_salt.as_bytes(),
        &argon_config,
    )
    .unwrap();

    web::block(move || {
        use config::schema::users::dsl::{email, first_name, id, last_name, password, users};
        diesel::insert_into(users)
            .values((
                id.eq(1),
                first_name.eq("Test"),
                last_name.eq("McTesty"),
                email.eq("test@mctesty.org"),
                password.eq(hashed_password),
            ))
            .get_result::<models::User>(&conn)
    })
    .await
    .unwrap()
}

/// Initializes configuration, overwriting the database name with a generated
/// database based on the name of the current thread.
///
/// This function attempts to load the config from the environment and falls
/// back to local default if it fails. This ensures it can use variables set in
/// CI for specific CI configuration.
///
/// Note that in either case, it will overwrite the database used to ensure
/// tests are properly isolated from one another.
fn init_app_config() -> config::App {
    let database_name = format!(
        "emcloud_test_{}_{}",
        std::process::id(),
        TEST_DATABASES_COUNT.fetch_add(1, atomic::Ordering::SeqCst)
    );

    if let Ok(mut config) = config::App::from_env() {
        config.pg.db = database_name;
        config
    } else {
        let pg = config::db::Config {
            user: "em".to_string(),
            password: "pass".to_string(),
            host: "localhost".to_string(),
            port: 5432,
            db: database_name,
        };

        config::App {
            host: "localhost".to_string(),
            port: 3000,
            secret_key: "very secure secret that shall protect the universe".to_string(),
            argon_salt: "I don't even have any idea for this one to be honest".to_string(),
            pg,
        }
    }
}

/// Returns an optimized configuration so hashing password is faster.
fn argon_config(secret: &[u8]) -> argon2::Config {
    argon2::Config {
        ad: &[],
        hash_length: 16,
        lanes: 1,
        mem_cost: 128,
        thread_mode: argon2::ThreadMode::Parallel,
        time_cost: 1,
        variant: argon2::Variant::Argon2id,
        version: argon2::Version::Version13,
        secret,
    }
}

fn pg_conn() -> diesel::PgConnection {
    let mut config = init_app_config();
    config.pg.db = "postgres".to_string();
    diesel::PgConnection::establish(&config.pg.url()).expect("could not establish dummy connection")
}

/// Creates a cookie with session information for the given user.
///
/// You must give the cookie to your request.
///
/// # Example
///
/// ```rust,ignore
/// #[actix_rt::test]
/// async fn test_endpoint() {
///     let (mut app, db) = test::init_app();
///     let conn = db.conn();
///     let user = test::create_user(&pool);
///     let cookie = test::auth_cookie(&user);
///
///     let request = test::get("/private").cookie(cookie).to_request();
///
///     // any asserts
/// }
///
/// ```
pub(crate) fn auth_cookie(user: &models::User) -> cookie::Cookie {
    let mut data = std::collections::HashMap::new();
    data.insert("user_id".to_string(), user.id.to_string());

    let config = init_app_config();
    let key = cookie::Key::derive_from(config.secret_key.as_bytes());
    let value = serde_json::to_string(&data).unwrap();
    let mut cookie = cookie::Cookie::new(config::COOKIE_NAME, value);
    cookie.set_expires(Some(
        time::OffsetDateTime::now_utc() + time::Duration::seconds(config::COOKIE_EXPIRY),
    ));

    let mut jar = cookie::CookieJar::new();
    jar.private(&key).add(cookie);

    let cookie = jar
        .delta()
        .find(|&c| c.name() == config::COOKIE_NAME)
        .unwrap();

    cookie.clone()
}

/// Creates a GET requests to the given URI.
///
/// Sets the default `Content-Type` header to `application/vnd.api+json`.
pub(crate) fn get(uri: &str) -> test::TestRequest {
    test::TestRequest::with_header("content-type", JSONAPI_CONTENT_TYPE)
        .method(http::Method::GET)
        .uri(uri)
}

/// Creates a POST requests to the given URI.
///
/// Sets the default `Content-Type` header to `application/vnd.api+json`.
pub(crate) fn post(uri: &str) -> test::TestRequest {
    test::TestRequest::with_header("content-type", JSONAPI_CONTENT_TYPE)
        .method(http::Method::POST)
        .uri(uri)
}

/// Creates a PATCH requests to the given URI.
///
/// Sets the default `Content-Type` header to `application/vnd.api+json`.
pub(crate) fn patch(uri: &str) -> test::TestRequest {
    test::TestRequest::with_header("content-type", JSONAPI_CONTENT_TYPE)
        .method(http::Method::PATCH)
        .uri(uri)
}

/// Creates a DELETE requests to the given URI.
///
/// Sets the default `Content-Type` header to `application/vnd.api+json`.
pub(crate) fn delete(uri: &str) -> test::TestRequest {
    test::TestRequest::with_header("content-type", JSONAPI_CONTENT_TYPE)
        .method(http::Method::DELETE)
        .uri(uri)
}

/// Easy-to-use macro to create JSON objects.
///
/// This relies on `serde_json::Value` so any of its compatible types are
/// supported.
macro_rules! json {
    { $($key:literal: $value:expr),+ $(,)? } => {{
        let mut map: serde_json::Map<String, serde_json::Value> = serde_json::Map::new();
        $(
            map.insert($key.into(), $value.into());
        )+
        map
    }};
}
