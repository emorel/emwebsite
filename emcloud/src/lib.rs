#![deny(clippy::pedantic)]

#[allow(unused_imports)]
#[macro_use]
extern crate log;
#[macro_use]
extern crate em_jsonapi;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use crate::traits::FromEnv;
use actix_web::middleware;
use actix_web::{App, HttpServer};

#[cfg(test)]
#[macro_use]
mod test;

/// Errors that may occur throughout the API.
#[macro_use]
mod error;
/// Exposes the endpoints of emCloud.
mod api;
/// Namespace for error middleware handlers.
mod catchers;
/// Namespace for the environment configuration.
mod config;
/// Namespace for all data models and JSON:API resources.
mod models;
/// Namespace for various services.
mod services;
/// Namespace for all traits.
mod traits;

pub use error::Error;
pub use error::Result;

/// Starts the API using environment configuration.
///
/// # Errors
///  * failed to bind to the given address
///
/// # Panics
///  * failed to load environment configuration
pub async fn run() -> std::io::Result<()> {
    config::logger::init();

    let config = config::App::from_env().expect("failed to load environment");
    let pool = config::db::init(&config.pg.url());
    let address = config.address();

    config::db::run_migrations(&pool).expect("failed to run migrations");

    HttpServer::new(move || {
        let secret = config.secret_key.as_bytes();

        // Note
        //  If changing the app configuration, you likely also want to change
        //  the test app configuration in order for any new, otherwise new
        //  configuration will not be tested !
        App::new()
            .data(config.clone())
            .data(pool.clone())
            .wrap(config::cors())
            .wrap(config::default_headers())
            .wrap(config::auth(secret))
            .wrap(config::error_handlers())
            .wrap(middleware::Logger::default())
            .configure(api::routes)
    })
    .bind(&address)?
    .run()
    .await
}
