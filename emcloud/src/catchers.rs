use actix_web::dev;
use actix_web::dev::MessageBody;
use actix_web::middleware::errhandlers::ErrorHandlerResponse;
use em_jsonapi::actix::JsonApiResponseError;

/// Sets a default body with a message consumable for the client for 404 errors.
#[allow(clippy::needless_pass_by_value)]
#[allow(clippy::unnecessary_wraps)]
pub fn not_found(
    response: dev::ServiceResponse<dev::Body>,
) -> actix_web::Result<ErrorHandlerResponse<dev::Body>> {
    let response = if has_body(&response) {
        response
    } else {
        let error = not_found!("Route was not found");
        dev::ServiceResponse::new(response.request().clone(), error.as_response_error())
    };

    Ok(ErrorHandlerResponse::Response(response))
}

/// Sets a default body with a message consumable for the client for 500 errors.
#[allow(clippy::needless_pass_by_value)]
#[allow(clippy::unnecessary_wraps)]
pub fn internal_error(
    response: dev::ServiceResponse<dev::Body>,
) -> actix_web::Result<ErrorHandlerResponse<dev::Body>> {
    let response = if has_body(&response) {
        response
    } else {
        let error = internal_error!("An unknown error occurred");
        dev::ServiceResponse::new(response.request().clone(), error.as_response_error())
    };

    Ok(ErrorHandlerResponse::Response(response))
}

fn has_body(response: &dev::ServiceResponse<dev::Body>) -> bool {
    !matches!(
        response.response().body().size(),
        dev::BodySize::None | dev::BodySize::Empty
    )
}
