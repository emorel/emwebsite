use crate::services::password;

use actix_web::http;
use actix_web::ResponseError;
use diesel::r2d2;
use em_jsonapi::actix::JsonApiResponseError;
use std::fmt;

pub type Result<T> = std::result::Result<T, Error>;

/// Any error that should be returned to the user.
///
/// This struct implements [`ResponseError`] to return an error to the end user.
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// A generic message with an error code used by [`bail!`].
    #[error("{0}")]
    Simple(String, http::StatusCode),

    /// Any error that may occur from Actix or Actix-specific libraries.
    #[error("{0}")]
    ActixWeb(#[from] actix_web::Error),

    /// Failure to retrieve a connection for the pool.
    #[error("{0}")]
    DbPool(#[from] r2d2::PoolError),

    #[error("{0}")]
    Password(#[from] password::Error),
}

/// Manual implementation because Diesel errors envelop anything from SQL syntax
/// errors to records not being found.
///
/// # TODO
///  - Currently does not differentiate database errors, which may include
///    constraint violations that should return a 400 Bad Request.
impl From<diesel::result::Error> for Error {
    fn from(error: diesel::result::Error) -> Self {
        match error {
            diesel::result::Error::NotFound => Self::Simple(
                "Record was not found".to_string(),
                http::StatusCode::NOT_FOUND,
            ),
            diesel::result::Error::QueryBuilderError(ref e)
            | diesel::result::Error::SerializationError(ref e) => {
                Self::Simple(e.to_string(), http::StatusCode::BAD_REQUEST)
            }
            _ => Self::Simple(
                "A server error occurred".to_string(),
                http::StatusCode::INTERNAL_SERVER_ERROR,
            ),
        }
    }
}

/// Manual implementation to extract the underlying error and convert from it
/// instead. Most of the time, this is probably going to be a Diesel error.
impl<T> From<actix_threadpool::BlockingError<T>> for Error
where
    T: fmt::Debug + Into<Error>,
{
    fn from(blocking_error: actix_threadpool::BlockingError<T>) -> Self {
        match blocking_error {
            actix_threadpool::BlockingError::Error(error) => error.into(),
            actix_threadpool::BlockingError::Canceled => Self::Simple(
                "A server error occurred".to_string(),
                http::StatusCode::INTERNAL_SERVER_ERROR,
            ),
        }
    }
}

impl JsonApiResponseError for Error {
    fn title(&self) -> Option<String> {
        Some(
            self.status_code()
                .canonical_reason()
                .unwrap_or("Unknown error")
                .to_string(),
        )
    }

    fn detail(&self) -> Option<String> {
        Some(self.to_string())
    }
}

impl ResponseError for Error {
    fn status_code(&self) -> http::StatusCode {
        match self {
            Self::Simple(_, status) => *status,
            Self::ActixWeb(error) => error.as_response_error().status_code(),
            Self::Password(error) => error.status_code(),
            Self::DbPool(_) => http::StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> actix_web::HttpResponse {
        self.as_response_error()
    }
}

macro_rules! bail {
    ($status:path, $msg:literal $(,)?) => {
        return Err($crate::Error::Simple($msg.to_string(), $status))
    };
    ($status:path, $($msg:expr),+ $(,)?) => {
        return Err($crate::Error::Simple(format!($($msg),+), $status))
    };
}

/// Creates a simple error with the given status code and message.
///
/// Unlike [`bail!`], this macro does not return early.
///
/// [`bail!`]: crate::bail
macro_rules! error {
    ($status:path, $msg:literal $(,)?) => {
        $crate::Error::Simple($msg.to_string(), $status)
    };
    ($status:path, $($msg:expr),+ $(,)?) => {
        $crate::Error::Simple(format!($($msg),+), $status)
    };
}

/// Creates a simple 404 Not Found error.
macro_rules! not_found {
    ($msg:literal) => {
        error!(actix_web::http::StatusCode::NOT_FOUND, $msg)
    };
    ($($msg:expr),+ $(,)?) => {
        error!(actix_web::http::StatusCode::NOT_FOUND, $($msg),+)
    };
}

/// Creates a simple 500 Internal Server error.
macro_rules! internal_error {
    ($msg:literal) => {
        error!(actix_web::http::StatusCode::INTERNAL_SERVER_ERROR, $msg)
    };
}
