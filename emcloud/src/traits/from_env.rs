use std::str;

pub trait FromEnv: Sized {
    fn from_env() -> Result<Self, Error>;

    fn get_var<T: str::FromStr>(var_name: &'static str) -> Result<T, Error> {
        std::env::var(var_name).map_or(Err(Error::MissingVariable(var_name)), |env_value| {
            match env_value.parse::<T>() {
                Ok(value) => Ok(value),
                Err(_) => Err(Error::ParseError(var_name)),
            }
        })
    }
}

/// Any error occurring when retrieving an environment variable.
#[derive(Debug, PartialEq, thiserror::Error)]
pub enum Error {
    /// The environment variable does not exist.
    #[error("environment variable not set: {0}")]
    MissingVariable(&'static str),

    /// The environment variable could not be casted to its type.
    #[error("environment variable could not be parsed: {0}")]
    ParseError(&'static str),
}

#[cfg(test)]
mod tests {
    use super::*;

    struct TestEnv {
        test_string: String,
        test_int: u8,
    }

    impl FromEnv for TestEnv {
        fn from_env() -> Result<Self, Error> {
            Ok(Self {
                test_string: Self::get_var("TEST_STRING")?,
                test_int: Self::get_var("TEST_INT")?,
            })
        }
    }

    #[test]
    fn can_get_var_without_error() {
        std::env::set_var("STRING", "a string");
        std::env::set_var("INTEGER", "42");

        assert_eq!(TestEnv::get_var("STRING"), Ok("a string".to_string()));
        assert_eq!(TestEnv::get_var("INTEGER"), Ok(42));
    }

    #[test]
    fn get_var_returns_variable_not_found() {
        assert_eq!(
            TestEnv::get_var::<String>("DO_NOT_EXIST"),
            Err(Error::MissingVariable("DO_NOT_EXIST"))
        );
    }

    #[test]
    fn get_var_returns_parse_error() {
        std::env::set_var("BAD_TYPE", "do not parse as int, fool !");

        assert_eq!(
            TestEnv::get_var::<u8>("BAD_TYPE"),
            Err(Error::ParseError("BAD_TYPE"))
        );
    }

    #[test]
    fn can_be_implemented() {
        std::env::set_var("TEST_STRING", "This is a string");
        std::env::set_var("TEST_INT", "42");

        let expected = TestEnv {
            test_string: "This is a string".to_string(),
            test_int: 42,
        };

        let actual = TestEnv::from_env().unwrap();

        assert_eq!(expected.test_string, actual.test_string);
        assert_eq!(expected.test_int, actual.test_int);
    }

    #[test]
    fn derives_without_prefix() {
        #[derive(em_derives::FromEnv)]
        struct MyConfig {
            a_string: String,
            an_int: u8,
        }

        std::env::set_var("A_STRING", "Hello world !");
        std::env::set_var("AN_INT", "42");

        let config = MyConfig::from_env().unwrap();

        assert_eq!(config.a_string, "Hello world !".to_string());
        assert_eq!(config.an_int, 42);
    }

    #[test]
    fn derives_with_prefix() {
        #[derive(em_derives::FromEnv)]
        #[env(prefix = "CONFIG")]
        struct MyConfig {
            a_string: String,
            an_int: u8,
        }

        std::env::set_var("CONFIG_A_STRING", "Hello world !");
        std::env::set_var("CONFIG_AN_INT", "42");

        let config = MyConfig::from_env().unwrap();

        assert_eq!(config.a_string, "Hello world !".to_string());
        assert_eq!(config.an_int, 42);
    }

    #[test]
    fn derives_with_default_for_missing_value() {
        #[derive(em_derives::FromEnv)]
        struct MyConfig {
            #[env(default = "Hello world !")]
            i_am_missing: String,
            an_int: u8,
        }

        std::env::set_var("AN_INT", "42");

        let config = MyConfig::from_env().unwrap();

        assert_eq!(config.i_am_missing, "Hello world !".to_string());
        assert_eq!(config.an_int, 42);
    }

    #[test]
    fn derives_with_default_for_set_value() {
        #[derive(em_derives::FromEnv)]
        struct MyConfig {
            #[env(default = "Hello world !")]
            a_string: String,
            an_int: u8,
        }

        std::env::set_var("A_STRING", "Bye world !");
        std::env::set_var("AN_INT", "42");

        let config = MyConfig::from_env().unwrap();

        assert_eq!(config.a_string, "Bye world !".to_string());
        assert_eq!(config.an_int, 42);
    }

    #[test]
    fn derives_with_nested_configuration_structs() {
        #[derive(em_derives::FromEnv)]
        struct SayHelloConfig {
            hello_type: String,

            #[env(nested)]
            to_whom: WorldConfig,
        }

        #[derive(em_derives::FromEnv)]
        #[env(prefix = "WORLD")]
        struct WorldConfig {
            hello_target: String,
        }

        std::env::set_var("HELLO_TYPE", "Hello");
        std::env::set_var("WORLD_HELLO_TARGET", "world");

        let config = SayHelloConfig::from_env().unwrap();
        let greeting = format!("{} {} !", config.hello_type, config.to_whom.hello_target);

        assert_eq!(greeting, "Hello world !".to_string());
    }
}
