use diesel::{r2d2, PgConnection};

pub type PgDatabase = r2d2::ConnectionManager<PgConnection>;
pub type PgPool = r2d2::Pool<PgDatabase>;

diesel_migrations::embed_migrations!();

#[derive(Debug, Clone, em_derives::FromEnv)]
#[env(prefix = "POSTGRES")]
pub struct Config {
    pub user: String,
    pub password: String,
    pub host: String,
    pub port: u16,
    pub db: String,
}

impl Config {
    #[must_use]
    pub fn url(&self) -> String {
        format!(
            "postgres://{}:{}@{}:{}/{}",
            self.user, self.password, self.host, self.port, self.db
        )
    }
}

/// Create a r2d2 database connection pool.
///
/// # Panics
///
///  * Panics if the the pool fails to be built.
#[must_use]
pub fn init(db_url: &str) -> PgPool {
    let manager = r2d2::ConnectionManager::<PgConnection>::new(db_url);
    PgPool::builder()
        .build(manager)
        .expect("failed to create db pool")
}

pub fn run_migrations(pool: &PgPool) -> Result<(), diesel_migrations::RunMigrationsError> {
    let conn = pool.get().unwrap();
    embedded_migrations::run(&conn)
}
