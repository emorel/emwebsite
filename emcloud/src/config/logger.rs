/// Initializes a logger.
///
/// # Panics
///
///  * Panics if it fails to initialize the logger.
pub fn init() {
    let config = simplelog::ConfigBuilder::new()
        .set_time_to_local(true)
        .build();

    simplelog::TermLogger::init(
        simplelog::LevelFilter::Debug,
        config,
        simplelog::TerminalMode::Mixed,
        simplelog::ColorChoice::Auto,
    )
    .expect("failed to init logger");
}
