table! {
    users (id) {
        id -> Int2,
        email -> Varchar,
        password -> Varchar,
        first_name -> Varchar,
        last_name -> Varchar,
        summary -> Nullable<Varchar>,
        description -> Nullable<Text>,
    }
}
