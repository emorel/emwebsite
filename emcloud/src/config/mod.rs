use actix_web::http::header;
use actix_web::middleware;
use actix_web::{dev, http};

use crate::catchers;

pub mod db;
pub mod logger;
pub mod schema;

pub use db::PgPool;

/// The name of the session cookie.
pub const COOKIE_NAME: &str = "em-session";

/// The expiry of the session cookie in seconds.
///
/// Currently, prolonging sessions does not work with max age sadly (#37).
///
/// [#37]: https://github.com/actix/actix-extras/issues/37
pub const COOKIE_EXPIRY: i64 = 1800;

/// The environment configuration for the API.
#[derive(Debug, Clone, em_derives::FromEnv)]
#[env(prefix = "EMCLOUD")]
pub struct App {
    #[env(default = "127.0.0.1")]
    pub host: String,
    #[env(default = 3000_u16)]
    pub port: u16,

    pub secret_key: String,
    pub argon_salt: String,

    #[env(nested)]
    pub pg: db::Config,
}

impl App {
    #[must_use]
    pub fn address(&self) -> String {
        format!("{}:{}", self.host, self.port)
    }
}

/// Sets up the CORS policy and returns the actix middleware.
#[must_use]
pub fn cors() -> actix_cors::Cors {
    actix_cors::Cors::default()
        // TODO: Allowed origin of CORS should probably be based off environment.
        .allowed_origin("http://localhost:8080")
        .allowed_methods(vec!["GET", "POST", "PATCH", "DELETE"])
        .allowed_headers(vec![header::CONTENT_TYPE, header::ACCEPT])
        .supports_credentials()
        .max_age(3600)
}

/// Sets the default headers.
///
/// This includes HTTP security headers.
#[must_use]
pub fn default_headers() -> middleware::DefaultHeaders {
    middleware::DefaultHeaders::new()
        .header(header::CACHE_CONTROL, "no-store")
        .header(header::CONTENT_SECURITY_POLICY, "frame-ancestors 'none'")
        .header(
            header::STRICT_TRANSPORT_SECURITY,
            "max-age=31536000; includeSubDomains",
        )
        .header(header::X_CONTENT_TYPE_OPTIONS, "nosniff")
        .header(header::X_FRAME_OPTIONS, "DENY")
}

/// Sets up the session middleware.
///
/// The session expires every `COOKIE_EXPIRY` seconds, but clients can request
/// prolongation of the session.
#[must_use]
pub fn auth(secret: &[u8]) -> actix_session::CookieSession {
    actix_session::CookieSession::private(secret)
        .name(COOKIE_NAME)
        .path("/")
        .secure(true)
        .http_only(true)
        .expires_in(COOKIE_EXPIRY)
        .same_site(actix_web::cookie::SameSite::Strict)
}

/// Sets up error handlers.
#[must_use]
pub fn error_handlers() -> middleware::errhandlers::ErrorHandlers<dev::Body> {
    middleware::errhandlers::ErrorHandlers::new()
        .handler(http::StatusCode::NOT_FOUND, catchers::not_found)
        .handler(
            http::StatusCode::INTERNAL_SERVER_ERROR,
            catchers::internal_error,
        )
}
