use crate::config::schema::users;
use diesel::QueryResult;

/// Structure of the user.
///
/// There may only be one user at a time, i.e. the administrator, yours truly.
///
/// # TODO
///   * add `summary` and `description` to JSON:API resource definition after
///   [#37] has been fixed.
///
/// [#37]: https://gitlab.com/emorel/emwebsite/-/issues/37
#[derive(Debug, diesel::Queryable, diesel::Identifiable)]
pub struct User {
    pub id: i16,
    pub email: String,
    pub password: String,
    pub first_name: String,
    pub last_name: String,
    pub summary: Option<String>,
    pub description: Option<String>,
}

jsonapi_resource!(User; |&self| {
    kind "users";
    id self.id;

    attrs email, first_name, last_name;
});

impl User {
    pub fn find_by_email(email: &str, conn: &diesel::PgConnection) -> QueryResult<User> {
        use crate::config::schema::users::dsl::{email as _email, users};
        use diesel::prelude::*;

        users.filter(_email.eq(email)).first::<User>(conn)
    }

    pub fn find_by_id(id: i16, conn: &diesel::PgConnection) -> QueryResult<User> {
        use crate::config::schema::users::dsl::{id as _id, users};
        use diesel::prelude::*;

        users.filter(_id.eq(id)).first::<User>(conn)
    }
}
