# emCloud

# Installation

## In Docker

See [here](https://gitlab.com/emorel/emwebsite).

## On the host

If you wish to run this module outside of a Docker container, ensure a local
instance of PostgreSQL is up and running.  
The stable toolchain of Rust must be installed.

For auto-reloading, you should install `cargo-watch`.

For configuring the various components, refer to the [Environment
variables](#environment-variables) section.

# Development

## Working with the Database

The project leverages the Rust ORM Diesel.

If you have installed the project on your host, I suggest you install the
dedicated CLI for easier database management.

If you have installed the project through docker, a dedicated Diesel CLI
is available inside the Docker image.  
For shorter commands, the repository makes available a `bin/diesel`
script which takes all allowed Diesel CLI commands and executes them within the
container context.

# Testing

Integration tests may require access to a database, which means these tests may
need to clean up after themselves.

Note that if a test does not require a database, then normal Rust tests are
recommended instead.

To do so, there are some helper functions available in the `test` module, it
notably creates a database for each test, which is garanteed to be dropped when
the test finishes (unless PogtreSQL crashes).

#### Example

```rust
use crate::test;
use actix_web::test as aw_test;
use actix_web::web;

#[actix_rt::test]
async fn my_integration_test() {
    // create a database for this test, and returns a fully configured
    // application and database to use
    let (mut app, db) = test::init_app().await;
    let user = test::create_user(&db).await;
    let cookie = test::auth_cookie(&user);
    let conn = db.pool().get().unwrap();

    // create arbitrary data for the test case
    web::block(move || {
        diesel::sql_query("INSERT INTO test_table (title) VALUES ('testing')").execute(&conn)
    })
    .await
    .unwrap();

    // perform a test request to the route
    let request = test::get("/my-route")
        .cookie(cookie)
        .to_request();

    let response = aw_test::call_service(&mut app, request).await;

    // perform asserts
    assert!(response.status().is_success())

    // `db` is dropped here, deleting the test database in the process
}
```

# Environment variables

```s
EMCLOUD_HOST = Host to listen to
EMCLOUD_PORT = Port to listen to
EMCLOUD_SECRET_KEY = A generated key to use for secret generations
EMCLOUD_ARGON_SALT = A random salt for password hashing

POSTGRES_DB = The PostgreSQL database name
POSTGRES_USER = The PostgreSQL database user name
POSTGRES_PASSWORD = The PostgreSQL database password
POSTGRES_HOST = The PostgreSQL host
POSTGRES_PORT = The PostgreSQL port
```
