use crate::Value;

use serde::ser::{SerializeMap, SerializeStruct};
use std::collections::HashSet;
use std::ops;

/// The `include` query parameter parsed as a vector of string from a query string
/// with comma-separated values.
/// Using a [`HashSet`] for the O(1) look-up.
pub type Includes = HashSet<String>;

/// The `filter` query parameter.
///
/// JSON:API is agnostic to filtering strategies supported by clients and
/// servers. As such, any possible JSON type may be used.
pub type Filter = Value;

/// The sort query parameter parsed from a query string with comma-separated
/// values.
#[derive(Debug, PartialEq)]
pub struct Sorts(pub Vec<Sort>);

impl ops::Deref for Sorts {
    type Target = Vec<Sort>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ops::DerefMut for Sorts {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl serde::Serialize for Sorts {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let sorts = self
            .iter()
            .map(Sort::to_signed_param)
            .collect::<Vec<String>>();

        serializer.serialize_str(&sorts.join(","))
    }
}

#[derive(Debug, PartialEq)]
pub enum Direction {
    Ascending,
    Descending,
}

/// A single sort parameter, with its order as a `u8`.
#[derive(Debug, PartialEq)]
pub struct Sort {
    pub field: String,
    pub order: Direction,
}

impl Sort {
    #[must_use]
    pub fn to_signed_param(&self) -> String {
        let sign = match self.order {
            Direction::Ascending => "-",
            Direction::Descending => "",
        };
        format!("{}{}", sign, self.field)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Fields(pub Vec<Field>);

impl Fields {
    #[must_use]
    pub fn has(&self, source: &str, name: &str) -> bool {
        let field = self.iter().find(|&field| field.source == source);

        match field {
            Some(field) => field.fields.get(name).is_some(),
            None => false,
        }
    }
}

impl ops::Deref for Fields {
    type Target = Vec<Field>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl ops::DerefMut for Fields {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl serde::Serialize for Fields {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;

        for field in self.iter() {
            let key = &field.source;
            let value = &field
                .fields
                .iter()
                // unfortunately need to clone
                // see https://github.com/rust-lang/rust/issues/83140
                .cloned()
                .collect::<Vec<String>>()
                .join(",");

            map.serialize_entry(key, value)?;
        }

        map.end()
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Field {
    pub source: String,
    pub fields: HashSet<String>,
}

#[derive(Debug, PartialEq)]
pub struct Page {
    pub size: u64,
    pub number: u64,
}

impl serde::Serialize for Page {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("Page", 2)?;

        state.serialize_field("size", &self.size)?;
        state.serialize_field("number", &self.number)?;

        state.end()
    }
}
