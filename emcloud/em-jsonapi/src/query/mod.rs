use crate::{Error, Value};

use serde::de;
use serde::ser::SerializeStruct;
use std::collections::{HashMap, HashSet};

pub mod params;

/// Representation of a JSON:API-compliant query string.
#[derive(Debug, PartialEq, Default)]
pub struct Query {
    pub includes: Option<params::Includes>,
    pub fields: Option<params::Fields>,
    pub sorts: Option<params::Sorts>,
    pub page: Option<params::Page>,
    pub filter: Option<params::Filter>,
}

impl Query {
    /// Creates a [`Query`] from a query string.
    ///
    /// # Errors
    ///
    /// * failed to deserialize
    pub fn from_params(q: &str) -> Result<Query, Error> {
        Ok(serde_qs::from_str(q)?)
    }

    /// Parses a [`Query`] into a query string.
    ///
    /// # Errors
    ///
    /// * failed to serialize
    pub fn to_string(&self) -> Result<String, Error> {
        Ok(serde_qs::to_string(self)?)
    }

    /// Checks if the given *field* has been requested by the client.
    ///
    /// If no fields parameter were sent by the client, returns `true`.
    #[must_use]
    pub fn include_field(&self, source: &str, field: &str) -> bool {
        match &self.fields {
            Some(fields) => fields.has(source, field),
            None => true,
        }
    }

    /// Checks if the given *relationship* has been requested by the client.
    ///
    /// If no included parameter were sent by the client, returns `false`.
    #[must_use]
    pub fn include_relationship(&self, relationship: &str) -> bool {
        match &self.includes {
            Some(included) => included.get(relationship).is_some(),
            None => false,
        }
    }
}

impl serde::Serialize for Query {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("Query", 5)?;

        if let Some(ref includes) = self.includes {
            state.serialize_field(
                "include",
                // unfortunately need to clone
                // see https://github.com/rust-lang/rust/issues/83140
                &includes.iter().cloned().collect::<Vec<String>>().join(","),
            )?;
        }

        if let Some(ref fields) = self.fields {
            state.serialize_field("fields", fields)?;
        }

        if let Some(ref sorts) = self.sorts {
            state.serialize_field("sort", sorts)?;
        }

        if let Some(ref page) = self.page {
            state.serialize_field("page", page)?;
        }

        if let Some(ref filter) = self.filter {
            state.serialize_field("filter", filter)?;
        }

        state.end()
    }
}

impl<'de> serde::Deserialize<'de> for Query {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        const ATTRS: &[&str] = &["includes", "fields", "sorts", "page", "filter"];

        #[derive(Debug, serde::Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        enum Attrs {
            Fields,
            Filter,
            Include,
            Page,
            Sort,
        }

        struct QueryVisitor;

        impl<'de> de::Visitor<'de> for QueryVisitor {
            type Value = Query;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(formatter, "an object containing query parameters")
            }

            fn visit_map<A>(self, mut access: A) -> Result<Self::Value, A::Error>
            where
                A: de::MapAccess<'de>,
            {
                let mut fields = None;
                let mut filter = None;
                let mut includes = None;
                let mut page = None;
                let mut sorts = None;

                while let Some(key) = access.next_key()? {
                    match key {
                        Attrs::Fields => {
                            let data = access.next_value::<HashMap<String, String>>()?;

                            fields = Some(params::Fields(
                                data.into_iter()
                                    .map(|(key, value)| {
                                        let values: HashSet<String> =
                                            value.split(',').map(String::from).collect();

                                        params::Field {
                                            source: key,
                                            fields: values,
                                        }
                                    })
                                    .collect(),
                            ));
                        }
                        Attrs::Include => {
                            let data = access.next_value::<String>()?;
                            includes = Some(data.split(',').map(String::from).collect());
                        }
                        Attrs::Filter => {
                            filter = Some(access.next_value::<Value>()?);
                        }
                        Attrs::Page => {
                            let data = access.next_value::<HashMap<String, u64>>()?;

                            page = Some(params::Page {
                                number: data["number"],
                                size: data["size"],
                            });
                        }
                        Attrs::Sort => {
                            let data = access.next_value::<String>()?;
                            sorts = Some(params::Sorts(
                                data.split(',')
                                    .map(|sort| params::Sort {
                                        field: sort.strip_prefix('-').unwrap_or(sort).to_string(),
                                        order: if sort.starts_with('-') {
                                            params::Direction::Descending
                                        } else {
                                            params::Direction::Ascending
                                        },
                                    })
                                    .collect(),
                            ));
                        }
                    }
                }

                Ok(Query {
                    includes,
                    fields,
                    sorts,
                    page,
                    filter,
                })
            }
        }

        deserializer.deserialize_struct("Query", ATTRS, QueryVisitor)
    }
}
