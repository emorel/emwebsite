//! This module keeps around some functions used in the implementation of the
//! JSON:API specifications. *They are not meant to be public API !*
#![doc(hidden)]

use crate::document::{Data, Resource, Resources};
use crate::Model;
use crate::Query;

/// Helper function to create [`crate::document::Data::Collection`]s from [`Iterator`] objects.
pub fn _iterator_to_collection<'a, M, T>(
    models: &mut T,
    query: Option<&Query>,
) -> (Data<Resource>, Option<Resources>)
where
    M: 'a + Model,
    T: Iterator<Item = &'a M>,
{
    match query {
        None => (
            Data::Collection(models.map(|model| model.to_resource()).collect()),
            None,
        ),
        Some(query) => {
            let mut resources: Resources = Vec::new();
            let mut all_included: Resources = Vec::new();

            for model in models {
                let (resource, included) = model.to_resource_with_query(query);

                resources.push(resource);

                if let Some(included) = included {
                    all_included.extend(included);
                }
            }

            (
                Data::Collection(resources),
                if query.includes.is_none() {
                    None
                } else {
                    Some(all_included)
                },
            )
        }
    }
}
