use crate::document::{PatchSet, PrimaryData};
use crate::{Error, Query};

use actix_web::dev;
use actix_web::web;
use actix_web::FromRequest;

use std::future::{ready, Future, Ready};
use std::pin::Pin;

impl FromRequest for Query {
    type Error = Error;

    type Future = Ready<Result<Self, Self::Error>>;

    type Config = ();

    fn from_request(req: &actix_web::HttpRequest, _: &mut dev::Payload) -> Self::Future {
        ready(Self::from_params(req.query_string()))
    }
}

impl<D> FromRequest for PatchSet<D>
where
    D: PrimaryData + serde::de::DeserializeOwned,
{
    type Error = Error;

    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    type Config = ();

    fn from_request(req: &actix_web::HttpRequest, payload: &mut dev::Payload) -> Self::Future {
        let bytes = web::Bytes::from_request(req, payload);

        Box::pin(async move {
            let bytes = bytes.await.unwrap();
            PatchSet::from_slice(&bytes)
        })
    }
}
