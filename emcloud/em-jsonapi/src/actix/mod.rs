//! This module adds support for [Actix Web](https://actix.rs/).
//!
//! It implements the [`actix_web::FromRequest`] trait for [`Query`] and exposes a
//! response builder [`JsonApiResponse`] implementing [`actix_web::Responder`].
//!
//! For patches and creations, it exposes the structure [`PatchSet`], implementing [`actix_web::FromRequest`]. These
//! also validate the correctness of the request body.
//!
//! For streamlining the API usage, it re-exports some other structures of the
//! library which are often used when building a response, such as the JSON:API
//! document [`Builder`].

mod extractors;
mod responders;

// re-exports to streamline actix API
pub use responders::{JsonApiResponse, JsonApiResponseError};
