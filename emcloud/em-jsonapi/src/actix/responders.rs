use crate::document;
use crate::{Document, Error, JSONAPI_CONTENT_TYPE};

use ::http::status;
use actix_web::cookie::Cookie;
use actix_web::dev::HttpResponseBuilder;
use actix_web::http;
use actix_web::{HttpResponse, Responder};

use std::collections::HashSet;
use std::future::{ready, Ready};

/// Public API to define JSON:API errors, all errors implementing this traits
/// can be used as response errors with Actix Web.
pub trait JsonApiResponseError: actix_web::ResponseError {
    fn title(&self) -> Option<String> {
        None
    }

    fn detail(&self) -> Option<String> {
        None
    }

    fn as_response_error(&self) -> HttpResponse {
        let error = Document::<document::Resource>::Err {
            errors: vec![document::ErrorObject {
                status: Some(self.status_code()),
                title: self.title().map(String::from),
                detail: self.detail().map(String::from),
                ..document::ErrorObject::default()
            }],
            meta: document::Meta::default(),
            links: document::Links::default(),
        };

        HttpResponseBuilder::new(self.status_code())
            .set_header(http::header::CONTENT_TYPE, JSONAPI_CONTENT_TYPE)
            .body(serde_json::to_string(&error).unwrap())
    }
}

impl actix_web::ResponseError for Error {
    fn status_code(&self) -> http::StatusCode {
        http::StatusCode::BAD_REQUEST
    }

    fn error_response(&self) -> HttpResponse {
        self.as_response_error()
    }
}

/// Implement crate errors to be returned as Bad Request errors.
impl JsonApiResponseError for Error {}

/// A JSON:API compliant response, implementing [`Responder`].
///
/// It encapsulate a [`HttpResponseBuilder`] for easier building, however only
/// a handful of its functions are available.
///
/// Given the specifications of JSON:API provide extensive documentation on
/// which status code should be returned under which conditions, this responder
/// does not require you to set a status code and will infer it from the request
/// as well as the [`Document`] provided if any.
///
/// As servers may respond with other status codes in all instances, you may
/// still set a status code manually, which will NOT be overridden.
///
/// [`Responder`]: actix_web::Responder
/// [`HttpResponseBuilder`]: actix_web::dev::HttpResponseBuilder
pub struct JsonApiResponse<T>
where
    T: document::PrimaryData + serde::Serialize,
{
    document: Option<Document<T>>,
    response: HttpResponseBuilder,
    status: Option<http::StatusCode>,
}

impl<T> JsonApiResponse<T>
where
    T: document::PrimaryData + serde::Serialize,
{
    /// Creates an empty JSON:API response with `204 No Content` status code.
    #[must_use]
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            response: HttpResponseBuilder::new(http::StatusCode::NO_CONTENT),
            document: None,
            status: None,
        }
    }

    /// Sets the JSON:API document to be used as body in the response.
    #[must_use]
    pub fn body(mut self, document: Document<T>) -> Self {
        self.document = Some(document);
        self
    }

    /// Sets the HTTP status of the response.
    ///
    /// Note that the status will NOT be overridden if set manually through this
    /// function.
    ///
    /// [`HttpResponseBuilder`]: actix_web::dev::HttpResponseBuilder
    #[must_use]
    pub fn status(mut self, status: http::StatusCode) -> Self {
        self.status = Some(status);
        self
    }

    /// Forwards to [`HttpResponseBuilder`].
    ///
    /// # Note
    /// It is important to note that forcing the content type header is not
    /// possible, as it is set when sending the response.
    ///
    /// [`HttpResponseBuilder`]: actix_web::dev::HttpResponseBuilder
    #[must_use]
    pub fn header<K, V>(mut self, key: K, value: V) -> Self
    where
        V: http::header::IntoHeaderValue,
        http::HeaderName: std::convert::From<K>,
    {
        self.response.header(key, value);
        self
    }

    /// Forwards to [`HttpResponseBuilder`].
    ///
    /// [`HttpResponseBuilder`]: actix_web::dev::HttpResponseBuilder
    #[must_use]
    pub fn cookie(mut self, cookie: Cookie) -> Self {
        self.response.cookie(cookie);
        self
    }

    /// Forwards to [`HttpResponseBuilder`].
    ///
    /// [`HttpResponseBuilder`]: actix_web::dev::HttpResponseBuilder
    #[must_use]
    pub fn del_cookie(mut self, cookie: &Cookie) -> Self {
        self.response.del_cookie(cookie);
        self
    }

    /// Infers the status code that should be used in an HTTP response based on
    /// the JSON:API specifications.
    ///
    /// # TODO
    ///  * Should test all possible inferred status codes
    fn infer_status_code(&self, req: &actix_web::HttpRequest) -> http::StatusCode {
        match &self.document {
            None => http::StatusCode::NO_CONTENT,
            Some(Document::Ok { .. }) => match *req.method() {
                http::Method::POST => http::StatusCode::CREATED,
                _ => http::StatusCode::OK,
            },
            Some(Document::Err { ref errors, .. }) => {
                let status_codes = errors
                    .iter()
                    .filter_map(|error| error.status)
                    .collect::<HashSet<status::StatusCode>>();

                if status_codes.len() == 1 {
                    *status_codes.iter().next().unwrap()
                } else {
                    http::StatusCode::BAD_REQUEST
                }
            }
        }
    }
}

impl<T> From<Document<T>> for JsonApiResponse<T>
where
    T: document::PrimaryData + serde::Serialize,
{
    fn from(document: Document<T>) -> Self {
        Self::new().body(document)
    }
}

impl<T> Responder for JsonApiResponse<T>
where
    T: document::PrimaryData + serde::Serialize,
{
    type Error = actix_web::Error;

    type Future = Ready<Result<HttpResponse, actix_web::Error>>;

    fn respond_to(self, req: &actix_web::HttpRequest) -> Self::Future {
        let status = self.status.unwrap_or_else(|| self.infer_status_code(req));

        let mut builder = self.response;

        builder.status(status);
        builder.header(http::header::CONTENT_TYPE, JSONAPI_CONTENT_TYPE);

        let response = if self.document.is_none() {
            builder.finish()
        } else {
            let body = serde_json::to_string(&self.document).unwrap();
            builder.body(body)
        };

        ready(Ok(response))
    }
}
