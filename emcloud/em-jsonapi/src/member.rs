use serde::de;
use std::convert::TryFrom;
use std::hash::Hash;
use std::ops;
use std::str::FromStr;

/// A key in a JSON:API Object.
///
/// This struct ensures some rules imposed by the specifications, notably
/// the restrictions on [reserved characters][reserved-specs] as well as
/// converting the keys into camel case as per [the recommendations][recommendations].
///
/// [reserved-specs]: https://jsonapi.org/format/#document-member-names-reserved-characters
/// [recommendations]: https://jsonapi.org/recommendations/#naming
#[derive(Debug, Clone, PartialEq, Eq, Hash, serde::Serialize)]
#[repr(transparent)]
pub struct Member(String);

impl TryFrom<&str> for Member {
    type Error = crate::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::from_str(value)
    }
}

impl FromStr for Member {
    type Err = crate::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // check if the slice starts or ends with illegal characters - a
        // whitespace, an underscore or a hyphen
        let illegal_chars: &[char] = &['\u{0020}', '\u{005F}', '\u{002D}'];
        if s.starts_with(illegal_chars) || s.ends_with(illegal_chars) {
            return Err(Self::Err::IllegalCharacter(
                illegal_chars
                    .iter()
                    .find(|&c| s.starts_with(*c) || s.ends_with(*c))
                    .unwrap()
                    .to_owned(),
                s.to_owned(),
            ));
        }

        let mut buf = String::with_capacity(s.len());
        let mut capitalize = false;

        for c in s.chars() {
            match c {
                '\u{002e}'
                | '\u{002f}'
                | '\u{0040}'
                | '\u{0060}'
                | '\u{0000}'..='\u{001f}'
                | '\u{0021}'..='\u{0029}'
                | '\u{002a}'..='\u{002c}'
                | '\u{003a}'..='\u{003f}'
                | '\u{005b}'..='\u{005e}'
                | '\u{007b}'..='\u{007f}' => {
                    return Err(Self::Err::ReservedMember(c, s.to_owned()))
                }
                _ => {
                    if c == '_' {
                        capitalize = true;
                    } else if capitalize {
                        buf.push(c.to_ascii_uppercase());
                        capitalize = false;
                    } else {
                        buf.push(c);
                    }
                }
            }
        }

        Ok(Self(buf))
    }
}

impl<'de> serde::Deserialize<'de> for Member {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct KeyVisitor;

        impl<'de> de::Visitor<'de> for KeyVisitor {
            type Value = Member;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("a valid JSON:API member")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                v.parse().map_err(de::Error::custom)
            }
        }

        deserializer.deserialize_str(KeyVisitor)
    }
}

impl ops::Deref for Member {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
