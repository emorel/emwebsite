/// All errors that can occur when processing data.
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// The query string sent by a client could not be deserialized.
    #[error("Invalid query string : {0}")]
    Query(#[from] serde_qs::Error),

    /// The payload sent by a client could not be deserialized.
    #[error("Invalid payload : {0}")]
    PatchSet(#[from] serde_json::Error),

    /// A member has a reserved character.
    #[error("Reserved character '{0}' in member '{1}'")]
    ReservedMember(char, String),

    /// A member starts or ends with an illegal character.
    #[error("Illegal character '{0}' in member '{1}'")]
    IllegalCharacter(char, String),
}

impl Error {
    /// Infers a title message to use in a [`ErrorObject`].
    ///
    /// For simplicity, it expects a character `:` to be present, and returns the
    /// substring *before* it.
    ///
    /// [`ErrorObject`]: crate::document::ErrorObject
    #[must_use]
    pub fn title(&self) -> Option<String> {
        let message = self.to_string();
        let delimiter = message.find(':');

        delimiter.map(|c| String::from(message[0..c].trim()))
    }

    /// Infers a detail message to use in a [`ErrorObject`].
    ///
    /// For simplicity, it expects a character `:` to be present, and returns the
    /// substring *after* it.
    ///
    /// [`ErrorObject`]: crate::document::ErrorObject
    #[must_use]
    pub fn detail(&self) -> Option<String> {
        let message = self.to_string();
        let delimiter = message.find(':');

        delimiter.map(|c| String::from(message[c + 1..].trim()))
    }
}
