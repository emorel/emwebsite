//! Exports a trait to be used to declare JSON:API resources.
//!
//! A macro is also available for easy resource declaration.

use crate::document::{
    Attributes, Data, Identifier, IntoPrimaryData, Links, Meta, Relationships, Resource, Resources,
};
use crate::utils;
use crate::Query;

pub trait Model {
    /// Returns the JSON:API `type` field for this resource.
    fn jsonapi_kind() -> String;

    /// Returns the JSON:API `id` field for this resource.
    fn jsonapi_id(&self) -> String;

    /// Maps all attributes to be rendered into a `HashMap`.
    ///
    /// May return `None` if no attributes should be rendered.
    fn jsonapi_attributes(&self, _: &Query) -> Attributes {
        Attributes::default()
    }

    /// May return all relationships defined for this model.
    ///
    /// If a [`Query`] is passed, it should also return all relationships as
    /// [`Resource`] objects.
    fn jsonapi_relationships(&self, _: &Query) -> (Option<Relationships>, Option<Resources>) {
        (None, None)
    }

    fn jsonapi_meta(&self) -> Option<Meta> {
        None
    }

    fn jsonapi_links(&self) -> Option<Links> {
        None
    }

    /// Transforms a model object into a [`Resource`].
    ///
    /// This function does not take into account any [`Query`] object. If it
    /// must take one into account, use [`Model::to_resource_with_query`]
    fn to_resource(&self) -> Resource {
        let query = Query::default();
        let (relationships, _) = self.jsonapi_relationships(&query);
        Resource {
            t: Self::jsonapi_kind(),
            id: self.jsonapi_id(),
            attributes: self.jsonapi_attributes(&query),
            links: self.jsonapi_links(),
            meta: self.jsonapi_meta(),
            relationships,
        }
    }

    fn to_identifier(&self) -> Identifier {
        Identifier {
            t: Self::jsonapi_kind(),
            id: self.jsonapi_id(),
        }
    }

    fn to_resource_with_query(&self, query: &Query) -> (Resource, Option<Resources>) {
        let (relationships, included) = self.jsonapi_relationships(query);
        (
            Resource {
                t: Self::jsonapi_kind(),
                id: self.jsonapi_id(),
                attributes: self.jsonapi_attributes(query),
                links: self.jsonapi_links(),
                meta: self.jsonapi_meta(),
                relationships,
            },
            included,
        )
    }
}

impl<T> IntoPrimaryData for T
where
    T: Model,
{
    fn to_primary_data(&self, query: Option<&Query>) -> (Data<Resource>, Option<Resources>) {
        query.map_or_else(
            || (Data::<Resource>::Member(Some(self.to_resource())), None),
            |query| {
                let (resource, included) = self.to_resource_with_query(query);
                (Data::<Resource>::Member(Some(resource)), included)
            },
        )
    }
}

impl<T, const N: usize> IntoPrimaryData for &[T; N]
where
    T: Model,
{
    fn to_primary_data(&self, query: Option<&Query>) -> (Data<Resource>, Option<Resources>) {
        utils::_iterator_to_collection(&mut self.iter(), query)
    }
}

impl<T> IntoPrimaryData for Vec<T>
where
    T: Model,
{
    fn to_primary_data(&self, query: Option<&Query>) -> (Data<Resource>, Option<Resources>) {
        utils::_iterator_to_collection(&mut self.iter(), query)
    }
}

impl<T> IntoPrimaryData for &Vec<T>
where
    T: Model,
{
    fn to_primary_data(&self, query: Option<&Query>) -> (Data<Resource>, Option<Resources>) {
        utils::_iterator_to_collection(&mut self.iter(), query)
    }
}

/// A DSL macro to implement a [`Model`] for a struct.
///
/// # TODO
///   * replace panics with errors
#[macro_export]
macro_rules! jsonapi_resource {
    ($model:ty; |&$self:ident| { $($tail:tt)* }) => {
        impl $crate::Model for $model {
            fn jsonapi_kind() -> String {
                extract_resource_kind!({ $($tail)* }).to_string()
            }

            fn jsonapi_id(&$self) -> String {
                extract_resource_id!({ $($tail)* }).to_string()
            }

            impl_resource_fragment!(@relation_fn $self, { $($tail)* });
            impl_resource_fragment!(@attrs_fn $self, { $($tail)* });
            impl_resource_fragment!(@meta_fn $self, { $($tail)* });
            impl_resource_fragment!(@links_fn $self, { $($tail)* });
        }
    }
}

#[doc(hidden)]
#[macro_export]
macro_rules! impl_resource_fragment {
    // attributes
    (@attrs_fn $self:ident, {
        attrs $($tail:tt)*
    }) => {
        fn jsonapi_attributes(&$self, query: &$crate::Query) -> $crate::document::Attributes {
            let mut _attributes = $crate::document::Attributes::default();
            impl_resource_fragment!(@attrs $self, _attributes, query, { attrs $($tail)* });
            _attributes
        }
    };

    (@attrs_fn $self:ident, {
        attr $($tail:tt)*
    }) => {
        fn jsonapi_attributes(&$self, query: query: &$crate::Query) -> $crate::document::Attributes {
            let mut _attributes = $crate::document::Attributes::default();
            impl_resource_fragment!(@attrs $self, _attributes, query, { attr $($tail)* });
            _attributes
        }
    };

    (@attrs $self:ident, $attributes:ident, $query:ident, {
        attrs $($fields:ident),+;
        $($tail:tt)*
    }) => {
        impl_resource_fragment!(@attrs $self, $attributes, $query, {
            $(
                attr stringify!($fields), { $self.$fields.to_owned() }
            )*
            $($tail)*
        })
    };

    (@attrs $self:ident, $attributes:ident, $query:ident, {
        attr $key:expr, $value:block
        $($tail:tt)*
    }) => {
        {
            if $query.include_field(&Self::jsonapi_kind(), $key) {
                use std::convert::TryInto;
                let key = $key.try_into().unwrap();
                let value: $crate::Value = $value.into();

                $attributes.insert(key, value);
            }
        }

        impl_resource_fragment!(@attrs $self, $attributes, $query, { $($tail)* })
    };

    // relationships
    (@relation_fn $self:ident, {
        has_many $($tail:tt)*
    }) => {
        fn jsonapi_relationships(&$self, query: &$crate::Query) -> (
            Option<$crate::document::Relationships>, Option<$crate::document::Resources>
        ) {
            let mut _relationships = $crate::document::Relationships::new();
            let mut _included = $crate::document::Resources::new();
            impl_resource_fragment!(@relation $self, _relationships, _included, query, { has_many $($tail)* });
            assert!(!_relationships.is_empty());
            (
                Some(_relationships),
                if query.includes.is_some() {
                    Some(_included)
                } else {
                    None
                }
            )
        }
    };

    (@relation_fn $self:ident, {
        has_one $($tail:tt)*
    }) => {
        fn jsonapi_relationships(&$self, query: &$crate::Query) -> (
            Option<$crate::document::Relationships>, Option<$crate::document::Resources>
        ) {
            let mut _relationships = $crate::document::Relationships::new();
            let mut _included = $crate::document::Resources::new();
            impl_resource_fragment!(@relation $self, _relationships, _included, query, { has_one $($tail)* });
            assert!(!_relationships.is_empty());
            (
                Some(_relationships),
                if query.includes.is_some() {
                    Some(_included)
                } else {
                    None
                }
            )
        }
    };

    (@relation $self:ident, $relationships:ident, $included:ident, $query:ident, {
        has_one $($fields:ident),*;
        $($tail:tt)*
    }) => {
        impl_resource_fragment!(@relation $self, $relationships, $included, $query, {
            $(has_one $fields, { data $self.$fields; })*
            $($tail)*
        })
    };

    (@relation $self:ident, $relationships:ident, $included:ident, $query:ident, {
        has_many $($fields:ident),*;
        $($tail:tt)*
    }) => {
        impl_resource_fragment!(@relation $self, $relationships, $included, $query, {
            $(has_many $fields, { data $self.$fields; })*
            $($tail)*
        })
    };

    (@relation $self:ident, $relationships:ident, $included:ident, $query:ident, {
        has_many $key:expr, { $($value:tt)* }
        $($tail:tt)*
    }) => {
        {
            use std::str::FromStr;
            let key = $crate::Member::from_str(stringify!($key)).unwrap();
            let value = impl_resource_fragment!(@has_many $self, { $($value)* });

            impl_resource_fragment!(@has_many_included $self, $included, $query, key, { $($value)* });

            $relationships.insert(key, value);
        }

        impl_resource_fragment!(@relation $self, $relationships, $included, $query, { $($tail)* })
    };

    (@relation $self:ident, $relationships:ident, $included:ident, $query:ident, {
        has_one $key:expr, { $($value:tt)* }
        $($tail:tt)*
    }) => {
        {
            use std::str::FromStr;
            let key = $crate::Member::from_str(stringify!($key)).unwrap();
            let value = impl_resource_fragment!(@has_one $self, { $($value)* });

            impl_resource_fragment!(@has_one_included $self, $included, $query, key, { $($value)* });

            $relationships.insert(key, value);
        }

        impl_resource_fragment!(@relation $self, $relationships, $included, $query, { $($tail)* })
    };

    (@has_many $self:ident, { $($value:tt)* }) => {{
        let data = impl_resource_fragment!(@has_many_data $self, { $($value)* });

        #[allow(unused_mut)]
        let mut links = $crate::document::Links::new();
        impl_resource_fragment!(@links $self, links, { $($value)* });

        #[allow(unused_mut)]
        let mut meta = $crate::document::Meta::new();
        impl_resource_fragment!(@meta $self, meta, { $($value)* });


        $crate::document::Relationship {
            data,
            // TODO: Find a better way to do this
            links: { !links.is_empty() }.then(|| links),
            meta: { !meta.is_empty() }.then(|| meta)
        }
    }};

    (@has_one $self:ident, { $($value:tt)* }) => {{
        let data = impl_resource_fragment!(@has_one_data $self, { $($value)* });

        #[allow(unused_mut)]
        let mut links = $crate::document::Links::new();
        impl_resource_fragment!(@links $self, links, { $($value)* });

        #[allow(unused_mut)]
        let mut meta = $crate::document::Meta::new();
        impl_resource_fragment!(@meta $self, meta, { $($value)* });


        $crate::document::Relationship {
            data,
            // TODO: Find a better way to do this
            links: { !links.is_empty() }.then(|| links),
            meta: { !meta.is_empty() }.then(|| meta)
        }
    }};

    // special cases for relationship data objects
    (@has_one_data $self:ident, {
        data $value:expr;
        $($tail:tt)*
    }) => {
        Some($crate::document::Data::Member(Some(
            $value.to_identifier()
        )))
    };

    (@has_one_data $self:ident, $($tail:tt)*) => { None };

    (@has_many_data $self:ident, {
        data $values:expr;
        $($tail:tt)*
    }) => {
        Some($crate::document::Data::Collection(
            $values.iter()
                .map(|association| association.to_identifier())
                .collect()
        ))
    };

    (@has_many_data $self:ident, $($tail:tt)*) => { None };

    (@has_many_included $self:ident, $included:ident, $query:ident, $key:ident, {
        data $values:expr;
        $($tail:tt)*
    }) => {
        if $query.include_relationship(&$key) {
            let resources = $values.iter()
                .map(|model| model.to_resource_with_query(&$query).0)
                .collect::<$crate::document::Resources>();

            $included.extend(resources)
        }
    };

    (@has_one_included $self:ident, $included:ident, $query:ident, $key:ident, {
        data $value:expr;
        $($tail:tt)*
    }) => {
        if $query.include_relationship(&$key) {
            let (resource, _) = $value.to_resource_with_query(&$query);
            $included.push(resource)
        }
    };

    // links
    (@links_fn $self:ident, {
        link $($tail:tt)*
    }) => {
        fn jsonapi_links(&$self) -> Option<$crate::document::Links> {
            let mut _links = $crate::document::Links::new();
            impl_resource_fragment!(@links $self, _links, { link $($tail)* });
            assert!(!_links.is_empty());
            Some(_links)
        }
    };

    (@links $self:ident, $links:ident, {
        link $key:expr, { $($value:tt)* }
        $($tail:tt)*
    }) => {
        {
            use std::convert::TryInto;
            let key = $key.try_into().unwrap();
            let value = impl_resource_fragment!(@link $self, { $($value)* });

            $links.insert(key, value);
        }

        impl_resource_fragment!(@links $self, $links, { $($tail)* })
    };

    (@links $self:ident, $links:ident, {
        link $key:expr, $value:expr;
        $($tail:tt)*
    }) => {
        impl_resource_fragment!(@links $self, $links, {
            link $key, { href { $value }; }
            $($tail)*
        })
    };

    (@link $self:ident, { href $href:block meta $($tail:tt)* }) => {{
        $crate::document::Link {
            href: $href,
            meta: {
                let mut _meta = $crate::document::Meta::new();
                impl_resource_fragment!(@meta $self, _meta, {
                    meta $($tail)*
                });
                assert!(!_meta.is_empty());
                Some(_meta)
            }
        }
    }};

    (@link $self:ident, { href $href:block }) => {{
        $crate::document::Link {
            href: $href,
            meta: None
        }
    }};

    (@link $self:ident, { href $href:expr; $($tail:tt)* }) => {
        impl_resource_fragment!(@link $self, { href { $href } $($tail)* });
    };

    // meta
    (@meta_fn $self:ident, {
        meta $($tail:tt)*
    }) => {
        fn jsonapi_meta(&$self) -> Option<$crate::document::Meta> {
            let mut _meta = $crate::document::Meta::new();
            impl_resource_fragment!(@meta $self, _meta, { meta $($tail)* });
            assert!(!_meta.is_empty());

            Some(_meta)
        }
    };

    (@meta $self:ident, $meta:ident, {
        meta $key:expr, $value:block
        $($tail:tt)*
    }) => {
        {
            use std::convert::TryInto;
            let key = $key.try_into().unwrap();
            let value: $crate::Value = $value.into();

            $meta.insert(key, value);
        }

        impl_resource_fragment!(@meta $self, $meta, { $($tail)* })
    };

    (@meta $self:ident, $meta:ident, {
        meta $key:expr, $value:expr;
        $($tail:tt)*
    }) => {
        impl_resource_fragment!(@meta $self, $meta, { meta $key, { $value } $($tail)* });
    };

    // munch tail for unmatched scopes

    // matches fragment scopes with unmatched tail
    (@$scope:ident $self:ident, $($args:ident),*, { $skip:tt $($tail:tt)* }) => {
        impl_resource_fragment!(@$scope $self, $($args),*, { $($tail)* });
    };

    // matches fn scopes with unmatched tail
    (@$scope:ident $self:ident, { $skip:tt $($tail:tt)* }) => {
        impl_resource_fragment!(@$scope $self, { $($tail)* });
    };

    // skip unmatched tail
    ($($tail:tt)*) => ();
}

#[doc(hidden)]
#[macro_export]
macro_rules! extract_resource_id {
    ({ id $value:expr; $($tail:tt)* }) => { $value };
    ({ $skip:tt $($tail:tt)*}) => {
        extract_resource_id!({ $($tail)* })
    };
    ($($tail:tt)*) => ();
}

#[doc(hidden)]
#[macro_export]
macro_rules! extract_resource_kind {
    ({ kind $value:expr; $($tail:tt)* }) => { $value };
    ({ $skip:tt $($tail:tt)*}) => {
        extract_resource_kind!({ $($tail)* })
    };
    ($($tail:tt)*) => ();
}
