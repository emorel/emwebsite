//! This lib is designed to easily define JSON:API resources and converting them
//! into JSON:API-compliant documents to be consumed by clients.

#![deny(clippy::pedantic)]

#[cfg(feature = "actix")]
pub mod actix;

pub mod document;
mod error;
mod member;
mod model;
pub mod query;
pub mod utils;

pub use document::Document;
pub use error::Error;
pub use member::Member;
pub use model::Model;
pub use query::Query;

pub type Value = serde_json::Value;

pub const JSONAPI_CONTENT_TYPE: &str = "application/vnd.api+json";
