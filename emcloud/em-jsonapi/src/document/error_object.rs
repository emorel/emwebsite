use crate::document::{Links, Meta};

use http::status;
use serde::ser::SerializeStruct;

pub type ErrorObjects = Vec<ErrorObject>;

#[derive(Debug, PartialEq)]
pub struct ErrorObject {
    pub id: Option<String>,
    pub links: Option<Links>,
    pub status: Option<status::StatusCode>,
    pub code: Option<String>,
    pub title: Option<String>,
    pub detail: Option<String>,
    pub source: Option<Source>,
    pub meta: Option<Meta>,
}

impl Default for ErrorObject {
    fn default() -> Self {
        Self {
            id: None,
            links: None,
            status: Some(status::StatusCode::INTERNAL_SERVER_ERROR),
            code: None,
            title: Some(String::from("Internal Server Error")),
            detail: Some(String::from("An error occurred. Please try again later.")),
            source: None,
            meta: None,
        }
    }
}

impl From<&str> for ErrorObject {
    fn from(error: &str) -> Self {
        Self {
            detail: Some(error.to_string()),
            ..ErrorObject::default()
        }
    }
}

impl serde::Serialize for ErrorObject {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("ErrorObject", 8)?;

        if let Some(ref id) = self.id {
            state.serialize_field("id", id)?;
        }

        if let Some(ref links) = self.links {
            state.serialize_field("links", links)?;
        }

        if let Some(ref status) = self.status {
            state.serialize_field("status", status.as_str())?;
        }

        if let Some(ref code) = self.code {
            state.serialize_field("code", code)?;
        }

        if let Some(ref title) = self.title {
            state.serialize_field("title", title)?;
        }

        if let Some(ref detail) = self.detail {
            state.serialize_field("detail", detail)?;
        }

        if let Some(ref source) = self.source {
            state.serialize_field("source", source)?;
        }

        if let Some(ref meta) = self.meta {
            state.serialize_field("meta", meta)?;
        }

        state.end()
    }
}

#[derive(Debug, serde::Serialize, PartialEq)]
pub struct Source {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub pointer: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub parameter: Option<String>,
}
