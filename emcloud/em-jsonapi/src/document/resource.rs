use crate::Value;
use crate::{document::Data, Member};

use serde::ser::SerializeStruct;
use std::collections::HashMap;

pub type Meta = HashMap<Member, Value>;
pub type Attributes = HashMap<Member, Value>;
pub type Resources = Vec<Resource>;
pub type Links = HashMap<Member, Link>;
pub type Relationships = HashMap<Member, Relationship>;

/// A JSON:API resource.
///
/// # TODO
///   Should validate at least one of attributes, relationships, links or meta
///   is present.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq)]
#[serde(deny_unknown_fields)]
pub struct Resource {
    /// The type of this JSON:API resource.
    #[serde(rename = "type")]
    pub t: String,

    /// The ID of this resource.
    pub id: String,

    /// All the data related to this resource.
    #[serde(skip_serializing_if = "Attributes::is_empty")]
    pub attributes: Attributes,

    /// All relationships related to this resource.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relationships: Option<Relationships>,

    /// Links related to this resource.
    ///
    /// When in a [`PatchSet`], this key is always `None`.
    ///
    /// [`PatchSet`]: crate::document::PatchSet
    #[serde(skip_serializing_if = "Option::is_none", skip_deserializing)]
    pub links: Option<Links>,

    /// Any non-standard meta information for this resource.
    ///
    /// When in a [`PatchSet`], this key is always `None`.
    ///
    /// [`PatchSet`]: crate::document::PatchSet
    #[serde(skip_serializing_if = "Option::is_none", skip_deserializing)]
    pub meta: Option<Meta>,
}

/// A resource identifier with only its type and id.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq)]
#[serde(deny_unknown_fields)]
pub struct Identifier {
    #[serde(rename = "type")]
    pub t: String,
    pub id: String,
}

/// Represents a relationship related to the primary resource.
///
/// # TODO: A relationship object requires 1 of its fields.
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq)]
pub struct Relationship {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub data: Option<Data<Identifier>>,

    #[serde(skip_serializing_if = "Option::is_none", skip_deserializing)]
    pub links: Option<Links>,

    #[serde(skip_serializing_if = "Option::is_none", skip_deserializing)]
    pub meta: Option<Meta>,
}

/// Represents a link in the JSON:API document.
///
/// A link is serialized as a simple JSON string if it does not have a [`Meta`].
#[derive(Debug, Clone, Default, PartialEq)]
pub struct Link {
    pub href: String,
    pub meta: Option<Meta>,
}

impl From<&str> for Link {
    fn from(s: &str) -> Self {
        Self::from(s.to_string())
    }
}

impl From<String> for Link {
    fn from(s: String) -> Self {
        Self {
            href: s,
            meta: None,
        }
    }
}

impl serde::Serialize for Link {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self.meta {
            // serialize as a JSON string if no meta object
            None => serializer.serialize_str(&self.href),
            Some(_) => {
                let mut state = serializer.serialize_struct("Link", 2)?;
                state.serialize_field("href", &self.href)?;
                state.serialize_field("meta", &self.meta)?;

                state.end()
            }
        }
    }
}

impl Link {
    #[must_use]
    pub fn new(href: &str) -> Link {
        Link {
            href: href.to_string(),
            meta: None,
        }
    }
}
