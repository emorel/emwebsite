use crate::document::{Data, IntoPrimaryData, Link, Links, Meta, Resource, Resources};
use crate::Value;
use crate::{Document, Query};

use std::convert::TryInto;

pub struct Builder<'a> {
    query: Option<&'a Query>,
    data: Data<Resource>,
    included: Option<Resources>,
    meta: Meta,
    links: Links,
}

impl<'a> Builder<'a> {
    #[must_use]
    pub fn new() -> Builder<'a> {
        Builder::default()
    }

    #[must_use]
    pub fn query(mut self, query: &'a Query) -> Builder {
        self.query = Some(query);
        self
    }

    pub fn data<M>(mut self, model: &'a M) -> Builder
    where
        M: IntoPrimaryData,
    {
        let (data, included) = model.to_primary_data(self.query);
        self.data = data;
        self.included = included;

        self
    }

    /// Adds a [`Meta`] object to the document.
    ///
    /// # Panics
    ///  * failed to parse key into a valid JSON:API member
    pub fn meta<V>(mut self, key: &'a str, value: V) -> Builder
    where
        V: Into<Value>,
    {
        self.meta.insert(key.try_into().unwrap(), value.into());
        self
    }

    /// Adds a [`Link`] object to the document.
    ///
    /// # Panics
    ///  * failed to parse key into a valid JSON:API member
    pub fn link<L>(mut self, key: &'a str, link: L) -> Builder
    where
        L: Into<Link>,
    {
        self.links.insert(key.try_into().unwrap(), link.into());
        self
    }

    /// Consumes the builder and creates a [`Document`] with its data.
    #[must_use]
    pub fn build(self) -> Document<Resource> {
        Document::Ok {
            data: self.data,
            meta: self.meta,
            links: self.links,
            included: self.included,
        }
    }
}

impl<'a> Default for Builder<'a> {
    fn default() -> Self {
        Builder {
            query: None,
            data: Data::<Resource>::Member(None),
            included: None,
            meta: Meta::new(),
            links: Links::new(),
        }
    }
}
