mod builder;
mod error_object;
mod resource;

pub use builder::Builder;
pub use error_object::*;
pub use resource::*;

use crate::{Error, Query, Value};

/// Restricts the types that can be used as primary data.
///
/// This trait is automatically implemented for structs implementing [`Model`].
///
/// [`Model`]: crate::Model
pub trait IntoPrimaryData: Sized {
    /// Performs the conversion to primary data.
    fn to_primary_data(&self, query: Option<&Query>) -> (Data<Resource>, Option<Resources>);

    /// Creates a new [`Document`] using `self` as primary data.
    ///
    /// This is a shortcut function and as such, it is limited and you cannot add
    /// document-level meta objects or links using this function.
    fn to_document(&self, query: Option<&Query>) -> Document<Resource> {
        query.map_or_else(
            || Builder::new().data(self).build(),
            |query| Builder::new().query(query).data(self).build(),
        )
    }
}

/// Marker to restrict primary data to specific internal types.
pub trait PrimaryData {}

#[derive(Debug, Clone, serde::Serialize, PartialEq)]
#[serde(untagged)]
pub enum Data<T> {
    Collection(Vec<T>),
    Member(Option<T>),
}

impl<'de, T> serde::Deserialize<'de> for Data<T>
where
    T: serde::Deserialize<'de>,
{
    /// Deserializes into a data object.
    ///
    /// This custom implementation follows the same principle as untagged enums
    /// deriving [`serde::Deserialize`] but keeps track of underlying errors to
    /// return them instead of being swallowed by serde.
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        use serde::de::Error;
        let value = Value::deserialize(deserializer)?;

        match Vec::<T>::deserialize(value.clone()) {
            Ok(collection) => Ok(Self::Collection(collection)),
            // TODO: There's probably a better way to check the type of the
            //  error but I could not find it in the docs
            Err(collection_err) if collection_err.to_string().contains("invalid type: null") => {
                Ok(Data::Member(None))
            }
            Err(collection_err) if collection_err.to_string().contains("invalid type: map") => {
                match T::deserialize(value) {
                    Ok(member) => Ok(Data::Member(Some(member))),
                    Err(member_err) => Err(D::Error::custom(member_err)),
                }
            }
            Err(collection_err) => Err(D::Error::custom(collection_err)),
        }
    }
}

#[derive(Debug, serde::Serialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
pub enum Document<T>
where
    T: PrimaryData,
{
    Ok {
        /// The primary data of the document.
        ///
        /// It must always be present if not an error document, but may be `null`.
        data: Data<T>,

        /// Any included relationships related to the primary data resource.
        ///
        /// It may be missing if not requested by the client.
        /// If it was requested by the client, but no relationships exist, it must return
        /// an empty array.
        #[serde(skip_serializing_if = "Option::is_none")]
        included: Option<Resources>,

        /// Any non-standard meta information about this document.
        ///
        /// **Note that this does NOT relate to the primary data, but the document itself.**
        #[serde(skip_serializing_if = "Meta::is_empty")]
        meta: Meta,

        /// Any link related to this document.
        ///
        /// **Note that this does NOT relate to the primary data, but the document itself.**
        #[serde(skip_serializing_if = "Links::is_empty")]
        links: Links,
    },

    Err {
        /// Any number of errors.
        ///
        /// Must always be present if an error document and must have *at least 1* error.
        errors: ErrorObjects,

        /// Any non-standard meta information about this document.
        ///
        /// **Note that this does NOT relate to any of the errors, but the document itself.**
        #[serde(skip_serializing_if = "Meta::is_empty")]
        meta: Meta,

        /// Any link related to this document.
        ///
        /// **Note that this does NOT relate to any of the errors, but the document itself.**
        #[serde(skip_serializing_if = "Links::is_empty")]
        links: Links,
    },
}

impl<T> Document<T>
where
    T: PrimaryData,
{
    #[must_use]
    pub fn is_ok(&self) -> bool {
        match *self {
            Document::Ok { .. } => true,
            Document::Err { .. } => false,
        }
    }

    #[must_use]
    pub fn is_err(&self) -> bool {
        match *self {
            Document::Ok { .. } => false,
            Document::Err { .. } => true,
        }
    }
}

#[derive(Debug, serde::Deserialize, PartialEq)]
#[serde(deny_unknown_fields)]
pub struct NewResource {
    #[serde(rename = "type")]
    pub t: String,

    pub id: Option<String>,

    pub attributes: Attributes,
}

#[derive(Debug, serde::Deserialize, PartialEq)]
pub struct PatchSet<T>
where
    T: PrimaryData,
{
    pub data: Data<T>,
}

impl<'de, T> PatchSet<T>
where
    T: PrimaryData + serde::Deserialize<'de>,
{
    /// Returns a patch set from a slice.
    ///
    /// # Errors
    ///
    /// * any deserialization error
    pub fn from_slice(bytes: &'de [u8]) -> Result<PatchSet<T>, Error> {
        Ok(serde_json::from_slice(bytes)?)
    }
}

impl PrimaryData for NewResource {}
impl PrimaryData for Resource {}
impl PrimaryData for Identifier {}
