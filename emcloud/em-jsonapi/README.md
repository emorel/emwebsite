# emCloud - jsonapi

A crate that implements the JSON:API specifications.

## Usage

```rust
#[macro_use]
extern crate em_jsonapi;

#[derive(Debug)]
struct Post {
    uuid: String,
    title: String,
    content: String,
}

#[derive(Debug)]
struct Blog {
    id: u64,
    title: String,
    posts: Vec<Post>,
}

jsonapi_resource!(Post; {
    kind "posts";
    id uuid;

    attrs title, content;

    attr "preview", {
        self.content
            .chars()
            .take(150)
            .collect::<String>()
    }
});

jsonapi_resource!(Blog; {
    kind "blogs";
    id id;

    has_many posts;

    meta "last_post_title", {
        self.get_last_post().title.to_owned()
    };

    link "self", {
        href format!("http://example.com/blogs/{}", self.id);
    }

    link "related", {
        href "http://example.com/blogs/{}/about", self.id;

        meta "desc", {
            String::from("Full description of the blog")
        }
    }
});

fn main() {
    let blog = Blog {
        id: 1,
        title: String::from("Cats are overrated"),
        posts: vec![
            Post {
                uuid: String::from("d7f03f2a-a5ef-11eb-bcbc-0242ac130002"),
                title: String::from("Why cats are the war machines of the future."),
                content: String::from("They have weapons for toes."),
            }
        ],
    };

    println!("{}", em_jsonapi::Document::from(blog));
}
```
