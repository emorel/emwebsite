use em_jsonapi::document::{self, Attributes};
use http::status;
use std::convert::TryInto;

mod helper;

#[test]
fn serializes_resource_with_optional_fields() {
    let links = {
        let mut hash = document::Links::new();
        hash.insert(
            "self".try_into().unwrap(),
            document::Link {
                href: "http://api.example.com/v1/articles/1".to_string(),
                meta: None,
            },
        );

        Some(hash)
    };

    let resource = document::Resource {
        t: "Article".into(),
        id: "1".into(),
        attributes: Attributes::default(),
        // TODO: Add relationships object
        relationships: None,
        links,
        // TODO: Add meta object
        meta: None,
    };

    let expected = String::from(
        r#"{"type":"Article","id":"1","links":{"self":"http://api.example.com/v1/articles/1"}}"#,
    );
    let actual = serde_json::to_string(&resource).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_resource_without_optional_fields() {
    let resource = document::Resource {
        t: "Article".into(),
        id: "1".into(),
        attributes: Attributes::default(),
        relationships: None,
        links: None,
        meta: None,
    };

    let expected = String::from(r#"{"type":"Article","id":"1"}"#);
    let actual = serde_json::to_string(&resource).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_link_without_optional_fields() {
    let link = document::Link {
        href: "http://api.example.com/articles/1".to_string(),
        meta: None,
    };

    // links without a meta object should serialize into a simple string value
    let expected = String::from(r#""http://api.example.com/articles/1""#);
    let actual = serde_json::to_string(&link).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_link_with_optional_fields() {
    let meta = {
        let mut hash = document::Meta::new();
        hash.insert("count".try_into().unwrap(), 1.into());

        Some(hash)
    };

    let link = document::Link {
        href: "http://api.example.com/articles/1".to_string(),
        meta,
    };

    let expected =
        String::from(r#"{"href":"http://api.example.com/articles/1","meta":{"count":1}}"#);
    let actual = serde_json::to_string(&link).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_error() {
    let error = document::ErrorObject {
        id: Some(1.to_string()),
        links: {
            let mut links = document::Links::new();
            links.insert(
                "about".try_into().unwrap(),
                document::Link {
                    href: "http://example.com/errors/1".to_string(),
                    meta: None,
                },
            );

            Some(links)
        },
        status: Some(status::StatusCode::NOT_FOUND),
        code: Some(404.to_string()),
        title: Some("Not Found".to_string()),
        detail: Some("The page you are looking does not exist".to_string()),
        source: None,
        meta: None,
    };

    let actual = &serde_json::to_string(&error).unwrap();
    let expected = r#"{"id":"1","links":{"about":"http://example.com/errors/1"},"status":"404","code":"404","title":"Not Found","detail":"The page you are looking does not exist"}"#;

    assert_json_eq!(actual, expected)
}
