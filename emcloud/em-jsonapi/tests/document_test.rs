#[macro_use]
extern crate em_jsonapi;

use em_jsonapi::document;
use em_jsonapi::document::{
    Attributes, Data, Document, Identifier, PatchSet, Relationship, Relationships, Resource,
};
use em_jsonapi::Model;

use std::convert::TryInto;

mod helper;

#[test]
fn serializes_document_with_optional_fields() {
    let links = {
        let mut hash = document::Links::new();
        hash.insert(
            "related".try_into().unwrap(),
            document::Link {
                meta: None,
                href: String::from("/test"),
            },
        );
        hash
    };

    let meta = {
        let mut hash = document::Meta::new();
        hash.insert("count".try_into().unwrap(), 0.into());
        hash
    };

    let document = Document::<Resource>::Ok {
        data: document::Data::Collection(vec![]),
        included: None,
        links,
        meta,
    };

    let expected = String::from(r#"{"data":[],"meta":{"count":0},"links":{"related":"/test"}}"#);
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_empty_document() {
    let document = Document::<Resource>::Ok {
        data: document::Data::Member(None),
        included: Default::default(),
        meta: Default::default(),
        links: Default::default(),
    };

    let expected = String::from(r#"{"data":null}"#);
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn serializes_with_macro() {
    #[derive(Debug, serde::Serialize)]
    struct User {
        id: i8,
        first_name: String,
        last_name: String,
    }

    impl User {
        fn friend_count(&self) -> i8 {
            0
        }
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs first_name, last_name;

        attr "full_name", {
            format!("{} {}", self.first_name, self.last_name)
        }

        link "self", format!("/users/{}", self.id);

        link "related", {
            href format!("/users/{}/friends", self.id);

            meta "desc", "Friends for this user";
            meta "count", self.friend_count();
        }

        meta "read-only", true;

        meta "copyright", {
            let year = 2021;
            format!("Copyright {} {} {}", year, self.first_name, self.last_name)
        }
    });

    let user = User {
        id: 1,
        first_name: String::from("Fingle"),
        last_name: String::from("Dan"),
    };

    let document = Document::Ok {
        data: document::Data::Member(Some(user.to_resource())),
        meta: Default::default(),
        included: Default::default(),
        links: Default::default(),
    };

    let expected = r#"{"data":{"type":"users","id":"1","attributes":{"firstName":"Fingle","lastName":"Dan","fullName":"Fingle Dan"},"links":{"self":"/users/1","related":{"href":"/users/1/friends","meta":{"count":0,"desc":"Friends for this user"}}},"meta":{"copyright":"Copyright 2021 Fingle Dan","read-only":true}}}"#;

    let actual = &serde_json::to_string(&document).unwrap();

    assert_json_eq!(actual, expected)
}

#[test]
fn deserializes_into_patch_set_from_resource() {
    let payload = r#"{
        "data": {
            "type":"users",
            "id":"1",
            "attributes": {
                "first_name":"Fingle",
                "last_name":"Dan"
            },
            "relationships": {
                "friends": {
                    "data": [
                        { "id":"2", "type":"users" }
                    ]
                },
                "best_friend": {
                    "data": { "id":"2", "type":"users" }
                }
            }
        }
    }"#;

    let actual: PatchSet<Resource> = serde_json::from_str(payload).unwrap();

    let expected = PatchSet {
        data: Data::Member(Some(Resource {
            t: String::from("users"),
            id: String::from("1"),
            attributes: hash!(Attributes; {
                first_name: "Fingle",
                last_name: "Dan"
            }),
            relationships: option_hash!(Relationships; {
                friends: Relationship {
                    data: Some(Data::Collection(vec![
                        Identifier { id: String::from("2"), t: String::from("users") }
                    ])),
                    links: None,
                    meta: None,
                },
                best_friend: Relationship {
                    data: Some(Data::Member(Some(Identifier { id: String::from("2"), t: String::from("users") }))),
                    links: None,
                    meta: None
                }
            }),
            meta: None,
            links: None,
        })),
    };

    assert_eq!(actual, expected)
}

#[test]
fn deserializes_into_patch_set_from_one_relationship_with_null_data() {
    let payload = r#"{ "data": null }"#;

    let expected = PatchSet {
        data: Data::Member(None),
    };

    let actual: PatchSet<Identifier> = serde_json::from_str(payload).unwrap();

    assert_eq!(actual, expected)
}

#[test]
fn deserializes_into_patch_set_from_one_relationship() {
    let payload = r#"{
        "data": { "id":"2", "type":"users" }
    }"#;

    let expected = PatchSet {
        data: Data::Member(Some(Identifier {
            t: String::from("users"),
            id: String::from("2"),
        })),
    };

    let actual: PatchSet<Identifier> = serde_json::from_str(payload).unwrap();

    assert_eq!(actual, expected)
}

#[test]
fn deserializes_into_patch_set_from_many_relationships_with_empty_data() {
    let payload = r#"{ "data": [] }"#;

    let expected = PatchSet {
        data: Data::Collection(Vec::new()),
    };

    let actual: PatchSet<Identifier> = serde_json::from_str(payload).unwrap();

    assert_eq!(actual, expected)
}

#[test]
fn deserializes_into_patch_set_from_many_relationships() {
    let payload = r#"{
        "data": [
            { "id":"2", "type":"users" },
            { "id":"3", "type":"users" }
        ]
    }"#;

    let expected = PatchSet {
        data: Data::Collection(vec![
            Identifier {
                t: String::from("users"),
                id: String::from("2"),
            },
            Identifier {
                t: String::from("users"),
                id: String::from("3"),
            },
        ]),
    };

    let actual: PatchSet<Identifier> = serde_json::from_str(payload).unwrap();

    assert_eq!(actual, expected)
}
