#[macro_use]
extern crate em_jsonapi;

use em_jsonapi::document::{
    Attributes, Data, Identifier, Link, Links, Meta, Relationship, Relationships, Resource,
};
use em_jsonapi::query::params::{Field, Fields};
use em_jsonapi::{Model, Query};
use std::convert::TryInto;

mod helper;

#[test]
fn model_to_resource() {
    #[derive(Debug)]
    struct User {
        identifier: String,
        name: String,
        age: u8,
    }

    impl Model for User {
        fn jsonapi_kind() -> String {
            String::from("users")
        }

        fn jsonapi_id(&self) -> String {
            self.identifier.to_string()
        }

        fn jsonapi_attributes(&self, _: &Query) -> Attributes {
            let mut attributes = Attributes::new();
            attributes.insert("name".try_into().unwrap(), self.name.to_owned().into());
            attributes.insert("age".try_into().unwrap(), self.age.to_owned().into());

            attributes
        }

        fn jsonapi_meta(&self) -> Option<Meta> {
            let mut meta = Meta::new();
            meta.insert(
                "note".try_into().unwrap(),
                "Actually not a bad driver.".into(),
            );
            Some(meta)
        }
    }

    let user = User {
        identifier: "Self-Proclaimed Best Driver".into(),
        name: "Fingle Dan".into(),
        age: 93,
    };

    let expected = Resource {
        t: "users".to_string(),
        id: "Self-Proclaimed Best Driver".to_string(),
        attributes: hash!(Attributes; {
            name: "Fingle Dan",
            age: 93
        }),
        relationships: None,
        links: None,
        meta: option_hash!(Meta; {
            note: "Actually not a bad driver."
        }),
    };

    let actual = user.to_resource();

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_minimal() {
    #[derive(Debug)]
    struct User {
        id: String,
        name: String,
        age: u8,
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs name, age;
    });

    let user = User {
        id: "Self-Proclaimed Best Driver".into(),
        name: "Fingle Dan".into(),
        age: 93,
    };

    let expected = Resource {
        t: "users".to_string(),
        id: "Self-Proclaimed Best Driver".to_string(),
        attributes: hash!(Attributes; {
            name: "Fingle Dan",
            age: 93
        }),
        relationships: None,
        links: None,
        meta: None,
    };

    let actual = user.to_resource();

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_with_query() {
    struct User {
        id: String,
        first_name: String,
        last_name: String,
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs first_name, last_name;

        attr "full_name", {
            format!("{} {}", self.first_name, self.last_name)
        };
    });

    let user = User {
        id: String::from("Self-Proclaimed Best Driver"),
        first_name: String::from("Fingle"),
        last_name: String::from("Dan"),
    };

    let expected = Resource {
        t: "users".to_string(),
        id: "Self-Proclaimed Best Driver".to_string(),
        attributes: hash!(Attributes; {
            firstName: "Fingle"
        }),
        relationships: None,
        links: None,
        meta: None,
    };

    let query = Query {
        fields: Some(Fields(vec![Field {
            source: "users".to_string(),
            fields: vec!["first_name".to_string()].into_iter().collect(),
        }])),
        ..Query::default()
    };

    let (actual, _) = user.to_resource_with_query(&query);

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_attributes() {
    #[derive(Debug)]
    struct User {
        id: String,
        first_name: String,
        last_name: String,
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs first_name, last_name;

        attr "full_name", {
            format!("{} {}", self.first_name, self.last_name)
        };
    });

    let user = User {
        id: String::from("Self-Proclaimed Best Driver"),
        first_name: String::from("Fingle"),
        last_name: String::from("Dan"),
    };

    let expected = Resource {
        t: "users".to_string(),
        id: "Self-Proclaimed Best Driver".to_string(),
        attributes: hash!(Attributes; {
            firstName: "Fingle",
            lastName: "Dan",
            fullName: "Fingle Dan"
        }),
        relationships: None,
        links: None,
        meta: None,
    };

    let actual = user.to_resource();

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_has_one() {
    #[derive(Debug)]
    struct Car {
        id: i8,
        name: String,
    }

    #[derive(Debug)]
    struct User {
        id: i8,
        name: String,
        sultan: Car,
        sand_king: Car,
        other_car: Car,
    }

    jsonapi_resource!(Car; |&self| {
        kind "cars";
        id self.id;

        attrs name;
    });

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs name;

        has_one sultan, sand_king;

        has_one other_car, {
            link "self", format!("/users/{}/relationships/other_car", self.id);

            link "related", {
                href format!("/users/{}/other_car", self.id);
                meta "brand", "Also a Sultan";
            }

            meta "note", "this is a meta note";
        }
    });

    let user = User {
        id: 1,
        name: String::from("Fingle Dan"),
        sultan: Car {
            id: 1,
            name: String::from("Fingle Sedan"),
        },
        sand_king: Car {
            id: 2,
            name: String::from("F1 Fingle"),
        },
        other_car: Car {
            id: 3,
            name: String::from("Don't remember"),
        },
    };

    let expected = Resource {
        t: String::from("users"),
        id: String::from("1"),
        relationships: option_hash!(Relationships; {
            other_car: Relationship {
                data: None,
                links: option_hash!(Links; {
                    self: Link {
                        href: "/users/1/relationships/other_car".to_string(),
                        meta: None
                    },
                    related: Link {
                        href: "/users/1/other_car".to_string(),
                        meta: option_hash!(Meta; {
                            brand: "Also a Sultan"
                        })
                    }
                }),
                meta: option_hash!(Meta; {
                    note: "this is a meta note"
                })
            },
            sultan: Relationship {
                data: Some(Data::Member(Some(
                    Identifier { id: String::from("1"), t: String::from("cars") }
                ))),
                links: None,
                meta: None
            },
            sand_king: Relationship {
                data: Some(Data::Member(Some(
                    Identifier { id: String::from("2"), t: String::from("cars") }
                ))),
                links: None,
                meta: None
            }
        }),
        attributes: hash!(Attributes; {
            name: "Fingle Dan"
        }),
        links: None,
        meta: None,
    };

    let actual = user.to_resource();

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_has_many() {
    #[derive(Debug)]
    struct Car {
        id: i8,
        name: String,
    }

    #[derive(Debug)]
    struct User {
        id: i8,
        name: String,
        cars: Vec<Car>,
        more_cars: Vec<Car>,
    }

    jsonapi_resource!(Car; |&self| {
        kind "cars";
        id self.id;

        attrs name;
    });

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs name;

        has_many more_cars;

        has_many cars, {
            link "self", format!("/users/{}/relationships/cars", self.id);

            link "related", {
                href format!("/users/{}/cars", self.id);
                meta "brands", "Multiple brands";
            }

            meta "note", "this is a meta note";
        }
    });

    let user = User {
        id: 1,
        name: String::from("Fingle Dan"),
        cars: vec![
            Car {
                id: 1,
                name: String::from("Fingle Sedan"),
            },
            Car {
                id: 2,
                name: String::from("F1 Fingle"),
            },
            Car {
                id: 3,
                name: String::from("Don't remember"),
            },
        ],
        more_cars: vec![
            Car {
                id: 4,
                name: String::from("Some other car"),
            },
            Car {
                id: 5,
                name: String::from("So many cars !"),
            },
        ],
    };

    let expected = Resource {
        t: String::from("users"),
        id: String::from("1"),
        relationships: option_hash!(Relationships; {
            cars: Relationship {
                data: None,
                links: option_hash!(Links; {
                    self: Link {
                        href: "/users/1/relationships/cars".to_string(),
                        meta: None
                    },
                    related: Link {
                        href: "/users/1/cars".to_string(),
                        meta: option_hash!(Meta; {
                            brands: "Multiple brands"
                        })
                    }
                }),
                meta: option_hash!(Meta; {
                    note: "this is a meta note"
                })
            },
            more_cars: Relationship {
                links: None,
                meta: None,
                data: Some(Data::Collection(vec![
                    Identifier {
                        t: String::from("cars"),
                        id: String::from("4")
                    },
                    Identifier {
                        t: String::from("cars"),
                        id: String::from("5")
                    }
                ]))
            }
        }),
        attributes: hash!(Attributes; {
            name: "Fingle Dan"
        }),
        links: None,
        meta: None,
    };

    let actual = user.to_resource();

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_link() {
    #[derive(Debug)]
    struct User {
        pub id: u8,
        pub name: String,
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs name;

        link "dummy", format!("http://example.com/testing/inline/{}", self.id);

        link "self", {
            href format!("http://example.com/users/{}", self.id);
        }

        link "related", {
            href {
                format!("http://example.com/users/{}/friends", self.id)
            };

            meta "desc", {
                String::from("Fingle Dan's friends")
            }

            meta "count", {
                0
            }
        }
    });

    let user = User {
        id: 1,
        name: String::from("Fingle Dan"),
    };

    let actual = user.to_resource();
    let expected = Resource {
        t: String::from("users"),
        id: 1.to_string(),
        attributes: hash!(Attributes; {
            name: "Fingle Dan"
        }),
        relationships: None,
        meta: None,
        links: option_hash!(Links; {
            self: Link {
                href: String::from("http://example.com/users/1"),
                meta: None
            },
            dummy: Link {
                href: String::from("http://example.com/testing/inline/1"),
                meta: None
            },
            related: Link {
                href: String::from("http://example.com/users/1/friends"),
                meta: option_hash!(Meta; {
                    desc: "Fingle Dan's friends",
                    count: 0
                })
            }
        }),
    };

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_meta() {
    #[derive(Debug)]
    struct User {
        pub id: u8,
        pub meta_attribute: String,
    }

    impl User {
        pub fn meta_message(&self) -> String {
            String::from("this meta is from a function")
        }
    }

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs meta_attribute;

        meta "note", "static str";

        meta "meta_from_attribute", {
            self.meta_attribute.to_owned()
        }

        meta "meta_from_fn", {
            self.meta_message()
        }
    });

    let user = User {
        id: 1,
        meta_attribute: String::from("this meta is from an attribute"),
    };

    let actual = user.to_resource();
    let expected = Resource {
        t: "users".into(),
        id: "1".into(),
        attributes: hash!(Attributes; {
            metaAttribute: user.meta_attribute.to_owned()
        }),
        relationships: None,
        links: None,
        meta: option_hash!(Meta; {
            note: "static str",
            metaFromFn: user.meta_message(),
            metaFromAttribute: user.meta_attribute
        }),
    };

    assert_eq!(actual, expected)
}

#[test]
fn jsonapi_resource_macro_full() {
    #[derive(Debug)]
    struct User {
        id: u8,
        first_name: String,
        last_name: String,
        blog: Blog,
        friends: Vec<Friend>,
    }

    #[derive(Debug)]
    struct Friend {
        user_id: u8,
        friend_id: u8,
    }

    #[derive(Debug)]
    struct Blog {
        uuid: String,
        title: String,
    }

    impl User {
        pub fn user_note(&self) -> &str {
            "Actually not a bad driver."
        }
    }

    jsonapi_resource!(Blog; |&self| {
        kind "blogs";
        id self.uuid;

        attrs title;
    });

    jsonapi_resource!(Friend; |&self| {
        kind "friends";
        id self.friend_id;

        attrs user_id;
    });

    jsonapi_resource!(User; |&self| {
        kind "users";
        id self.id;

        attrs first_name, last_name;

        has_one blog;

        has_many friends, {
            data self.friends;

            link "self", format!("/users/{}/relationships/friends", self.id);

            link "related", {
                href format!("/users/{}/friends", self.id);

                meta "count", {
                    self.friends.len()
                }
            }

            meta "desc", "All users associated to this user as friends.";
        }

        attr "full_name", {
            format!("{} {}", self.first_name, self.last_name)
        }

        link "self", String::from("/users/me");

        meta "note", {
            self.user_note()
        }

        meta "about", {
            "Some driver"
        }
    });

    let blog = Blog {
        uuid: String::from("d7f03f2a-a5ef-11eb-bcbc-0242ac130002"),
        title: String::from("Cats are overrated"),
    };

    let friend = Friend {
        user_id: 1,
        friend_id: 2,
    };

    let user = User {
        id: 1,
        first_name: String::from("Fingle"),
        last_name: String::from("Dan"),
        blog,
        friends: vec![friend],
    };

    let expected = Resource {
        t: "users".to_string(),
        id: "1".to_string(),
        attributes: hash!(Attributes; {
            firstName: "Fingle",
            lastName: "Dan",
            fullName: "Fingle Dan"
        }),
        relationships: option_hash!(Relationships; {
            blog: Relationship {
                links: None,
                meta: None,
                data: Some(Data::Member(Some(
                    Identifier {
                        t: String::from("blogs"),
                        id: String::from("d7f03f2a-a5ef-11eb-bcbc-0242ac130002"),
                    }
                )))
            },
            friends: Relationship {
                links: option_hash!(Links; {
                    self: Link {
                        href: "/users/1/relationships/friends".into(),
                        meta: None
                    },
                    related: Link {
                        href: "/users/1/friends".into(),
                        meta: option_hash!(Meta; {
                            count: 1
                        })
                    }
                }),
                meta: option_hash!(Meta; {
                    desc: "All users associated to this user as friends."
                }),
                data: Some(Data::Collection(vec![
                    Identifier {
                        t: String::from("friends"),
                        id: String::from("2"),
                    }
                ]))
            }
        }),
        links: option_hash!(Links; {
            self: Link {
                href: "/users/me".into(),
                meta: None
            }
        }),
        meta: option_hash!(Meta; {
            note: "Actually not a bad driver.",
            about: "Some driver"
        }),
    };

    let actual = user.to_resource();

    // breaking down asserts for easier debugging
    assert_eq!(actual.id, expected.id);
    assert_eq!(actual.t, expected.t);
    assert_eq!(actual.attributes, expected.attributes);
    assert_eq!(actual.meta, expected.meta);
    assert_eq!(actual.relationships, expected.relationships);
    assert_eq!(actual.links, expected.links);
}
