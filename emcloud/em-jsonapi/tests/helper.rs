#![allow(dead_code)]

use std::io::prelude::*;

use std::fs::File;
use std::path::Path;

#[macro_export]
macro_rules! assert_json_eq {
    ($json1:expr, $json2:expr) => {{
        let json1: serde_json::Value = serde_json::from_str($json1).unwrap();
        let json2: serde_json::Value = serde_json::from_str($json2).unwrap();

        assert_eq!(json1, json2)
    }};
}

// stupid slow but for testing ¯\_(ツ)_/¯
#[macro_export]
macro_rules! assert_json_unordered_eq {
    ($arr1:expr, $arr2:expr) => {{
        let json1: String = serde_json::to_string($arr1).unwrap();
        let json2: String = serde_json::to_string($arr2).unwrap();

        assert_json_eq!(&json1, &json2)
    }};
}

#[macro_export]
macro_rules! assert_qs_unordered_eq {
    ($qs1:expr, $qs2:expr) => {{
        let value1: serde_json::Value = serde_qs::from_str($qs1).unwrap();
        let value2: serde_json::Value = serde_qs::from_str($qs2).unwrap();

        assert_eq!(value1, value2)
    }};
}

#[macro_export]
macro_rules! option_hash {
    ($type:ty; $($tail:tt)*) => { Some(hash!($type; $($tail)*)) };
}

#[macro_export]
macro_rules! hash {
    ($type:ty; {
        $($key:ident: $val:expr),+
    }) => {{
        let mut fragment = <$type>::new();
        $(
            fragment.insert(stringify!($key).try_into().unwrap(), $val.into());
        )+

        fragment
    }};
}

pub fn load_json(filename: &str) -> String {
    let filename = format!("examples/{}", filename);
    let path = Path::new(&filename);
    let path_display = path.display();

    let mut file = match File::open(&path) {
        Ok(file) => file,
        Err(error) => panic!("Could not open file {}: {}", path_display, &error),
    };

    let mut buffer = String::new();

    if let Err(error) = file.read_to_string(&mut buffer) {
        panic!("Could not read file {} : {}", path_display, &error)
    }

    minify_json(&mut buffer);

    buffer
}

fn minify_json(json: &mut String) {
    let mut is_json_string = false;

    json.retain(|c| match c {
        ' ' | '\n' | '\r' => is_json_string,
        '"' => {
            is_json_string = !is_json_string;
            true
        }
        _ => true,
    });
}
