//! FIXME

// #[macro_use]
// mod helper;

// use em_jsonapi::{
//     query::params::{self, Fields, Sorts},
//     Query,
// };

// fn build_query<'a>() -> (Query, &'a str) {
//     (
//         Query {
//             includes: Some(vec![String::from("comments"), String::from("author")].into_iter().collect()),
//             fields: Some(
//                 Fields(
//                     vec![
//                         params::Field {
//                             source: String::from("articles"),
//                             fields: vec![String::from("title"), String::from("preview"), String::from("author")].into_iter().collect(),
//                         },
//                         params::Field {
//                             source: String::from("comments"),
//                             fields: vec![String::from("content"), String::from("author")].into_iter().collect(),
//                         },
//                     ]
//                 )
//             ),
//             sorts: Some(
//                 Sorts(
//                     vec![
//                         params::Sort {
//                             order: -1,
//                             field: String::from("title"),
//                         },
//                         params::Sort {
//                             order: 1,
//                             field: String::from("author")
//                         }
//                     ]
//                 ),
//             ),
//             page: Some(params::Page {
//                 size: 25,
//                 number: 2,
//             }),
//             filter: Some(
//                 serde_json::json!({
//                     "users.name": "Fingle Dan"
//                 })
//             ),
//         },

//         "include=comments%2Cauthor&fields[articles]=title%2Cpreview%2Cauthor&fields[comments]=content%2Cauthor&sort=-title%2Cauthor&page[size]=25&page[number]=2&filter[users.name]=Fingle+Dan"
//     )
// }

// #[test]
// fn serializes_to_query_string() {
//     let (q, expected) = build_query();

//     let actual = q.to_string().unwrap();

//     assert_qs_unordered_eq!(&actual, &expected)
// }

// #[test]
// fn deserializes_from_query_string() {
//     let (expected, q) = build_query();

//     let actual = Query::from_params(q).unwrap();

//     assert_json_unordered_eq!(&actual, &expected)
// }
