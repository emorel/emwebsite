#[macro_use]
extern crate em_jsonapi;

use em_jsonapi::document;
use em_jsonapi::{Document, Model, Query};
use http::status;
use std::convert::TryInto;

mod helper;

#[test]
fn single_resource_document_example() {
    let attributes = {
        let mut hash = document::Attributes::new();
        hash.insert(
            "title".try_into().unwrap(),
            "JSON:API paints my bike shed!".into(),
        );

        hash
    };

    let relationships = {
        let author_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/author".to_string(),
            meta: None,
        };
        let author_related_link = document::Link {
            href: "http://example.com/articles/1/author".to_string(),
            meta: None,
        };
        let comments_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/comments".to_string(),
            meta: None,
        };
        let comments_related_link = document::Link {
            href: "http://example.com/articles/1/comments".to_string(),
            meta: None,
        };

        let mut hash = document::Relationships::new();
        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), author_self_link);
        links.insert("related".try_into().unwrap(), author_related_link);
        hash.insert(
            "author".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: None,
                meta: None,
            },
        );

        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), comments_self_link);
        links.insert("related".try_into().unwrap(), comments_related_link);
        hash.insert(
            "comments".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: None,
                meta: None,
            },
        );

        Some(hash)
    };

    let document = Document::Ok {
        data: document::Data::Member(Some(document::Resource {
            t: "articles".into(),
            id: 1.to_string(),
            attributes,
            relationships,
            links: None,
            meta: None,
        })),
        included: Default::default(),
        meta: Default::default(),
        links: Default::default(),
    };

    let expected = helper::load_json("single-resource-document.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn multiple_resource_document_example() {
    let resource_object1 = document::Resource {
        t: "articles".into(),
        id: 1.to_string(),
        meta: None,
        attributes: {
            let mut attributes = document::Attributes::new();
            attributes.insert(
                "title".try_into().unwrap(),
                "JSON:API paints my bike shed!".into(),
            );

            attributes
        },
        relationships: {
            let author_self_link = document::Link {
                href: "http://example.com/articles/1/relationships/author".to_string(),
                meta: None,
            };
            let author_related_link = document::Link {
                href: "http://example.com/articles/1/author".to_string(),
                meta: None,
            };

            let mut relationships = document::Relationships::new();
            let mut links = document::Links::new();
            links.insert("self".try_into().unwrap(), author_self_link);
            links.insert("related".try_into().unwrap(), author_related_link);
            relationships.insert(
                "author".try_into().unwrap(),
                document::Relationship {
                    links: Some(links),
                    data: None,
                    meta: None,
                },
            );

            Some(relationships)
        },
        links: {
            let mut links = document::Links::new();
            links.insert(
                "self".try_into().unwrap(),
                document::Link {
                    href: "http://example.com/articles/1".to_string(),
                    meta: None,
                },
            );

            Some(links)
        },
    };

    let resource_object2 = document::Resource {
        t: "articles".into(),
        id: 2.to_string(),
        meta: None,
        attributes: {
            let mut attributes = document::Attributes::new();
            attributes.insert(
                "title".try_into().unwrap(),
                "JSON:API builds my bike shed!".into(),
            );

            attributes
        },
        relationships: {
            let author_self_link = document::Link {
                href: "http://example.com/articles/2/relationships/author".to_string(),
                meta: None,
            };
            let author_related_link = document::Link {
                href: "http://example.com/articles/2/author".to_string(),
                meta: None,
            };

            let mut relationships = document::Relationships::new();
            let mut links = document::Links::new();
            links.insert("self".try_into().unwrap(), author_self_link);
            links.insert("related".try_into().unwrap(), author_related_link);
            relationships.insert(
                "author".try_into().unwrap(),
                document::Relationship {
                    links: Some(links),
                    data: None,
                    meta: None,
                },
            );

            Some(relationships)
        },
        links: {
            let mut links = document::Links::new();
            links.insert(
                "self".try_into().unwrap(),
                document::Link {
                    href: "http://example.com/articles/2".to_string(),
                    meta: None,
                },
            );

            Some(links)
        },
    };

    let document = Document::Ok {
        data: document::Data::Collection(vec![resource_object1, resource_object2]),
        included: Default::default(),
        meta: Default::default(),
        links: Default::default(),
    };

    let expected = helper::load_json("multiple-resource-document.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn single_resource_compound_document_example() {
    let attributes = {
        let mut hash = document::Attributes::new();
        hash.insert(
            "title".try_into().unwrap(),
            "JSON:API paints my bike shed!".into(),
        );

        hash
    };

    let relationships = {
        let author_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/author".to_string(),
            meta: None,
        };
        let author_related_link = document::Link {
            href: "http://example.com/articles/1/author".to_string(),
            meta: None,
        };
        let comments_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/comments".to_string(),
            meta: None,
        };
        let comments_related_link = document::Link {
            href: "http://example.com/articles/1/comments".to_string(),
            meta: None,
        };

        let mut hash = document::Relationships::new();
        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), author_self_link);
        links.insert("related".try_into().unwrap(), author_related_link);
        hash.insert(
            "author".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: Some(document::Data::Member(Some(document::Identifier {
                    t: "people".to_string(),
                    id: 9.to_string(),
                }))),
                meta: None,
            },
        );

        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), comments_self_link);
        links.insert("related".try_into().unwrap(), comments_related_link);
        hash.insert(
            "comments".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: Some(document::Data::Collection(vec![
                    document::Identifier {
                        t: "comments".to_string(),
                        id: 5.to_string(),
                    },
                    document::Identifier {
                        t: "comments".to_string(),
                        id: 12.to_string(),
                    },
                ])),
                meta: None,
            },
        );

        Some(hash)
    };

    let included = {
        Some(vec![
            document::Resource {
                t: "people".to_string(),
                id: 9.to_string(),
                relationships: None,
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("firstName".try_into().unwrap(), "Fingle".into());
                    attributes.insert("lastName".try_into().unwrap(), "Dan".into());
                    attributes.insert("twitter".try_into().unwrap(), "fingularity".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/people/9".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
            document::Resource {
                t: "comments".to_string(),
                id: 5.to_string(),
                relationships: {
                    let mut relationships = document::Relationships::new();
                    relationships.insert(
                        "author".try_into().unwrap(),
                        document::Relationship {
                            meta: None,
                            links: None,
                            data: Some(document::Data::Member(Some(document::Identifier {
                                t: "people".to_string(),
                                id: 2.to_string(),
                            }))),
                        },
                    );

                    Some(relationships)
                },
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("body".try_into().unwrap(), "First!".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/comments/5".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
            document::Resource {
                t: "comments".to_string(),
                id: 12.to_string(),
                relationships: {
                    let mut relationships = document::Relationships::new();
                    relationships.insert(
                        "author".try_into().unwrap(),
                        document::Relationship {
                            meta: None,
                            links: None,
                            data: Some(document::Data::Member(Some(document::Identifier {
                                t: "people".to_string(),
                                id: 9.to_string(),
                            }))),
                        },
                    );

                    Some(relationships)
                },
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("body".try_into().unwrap(), "I like XML better".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/comments/12".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
        ])
    };

    let document = Document::Ok {
        data: document::Data::Member(Some(document::Resource {
            t: "articles".into(),
            id: 1.to_string(),
            attributes,
            relationships,
            links: None,
            meta: None,
        })),
        included,
        meta: Default::default(),
        links: Default::default(),
    };

    let expected = helper::load_json("single-resource-compound-document.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn multiple_resource_compound_document_example() {
    let attributes = {
        let mut hash = document::Attributes::new();
        hash.insert(
            "title".try_into().unwrap(),
            "JSON:API paints my bike shed!".into(),
        );

        hash
    };

    let relationships = {
        let author_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/author".to_string(),
            meta: None,
        };
        let author_related_link = document::Link {
            href: "http://example.com/articles/1/author".to_string(),
            meta: None,
        };
        let comments_self_link = document::Link {
            href: "http://example.com/articles/1/relationships/comments".to_string(),
            meta: None,
        };
        let comments_related_link = document::Link {
            href: "http://example.com/articles/1/comments".to_string(),
            meta: None,
        };

        let mut hash = document::Relationships::new();
        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), author_self_link);
        links.insert("related".try_into().unwrap(), author_related_link);
        hash.insert(
            "author".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: Some(document::Data::Member(Some(document::Identifier {
                    t: "people".to_string(),
                    id: 9.to_string(),
                }))),
                meta: None,
            },
        );

        let mut links = document::Links::new();
        links.insert("self".try_into().unwrap(), comments_self_link);
        links.insert("related".try_into().unwrap(), comments_related_link);
        hash.insert(
            "comments".try_into().unwrap(),
            document::Relationship {
                links: Some(links),
                data: Some(document::Data::Collection(vec![
                    document::Identifier {
                        t: "comments".to_string(),
                        id: 5.to_string(),
                    },
                    document::Identifier {
                        t: "comments".to_string(),
                        id: 12.to_string(),
                    },
                ])),
                meta: None,
            },
        );

        Some(hash)
    };

    let included = {
        Some(vec![
            document::Resource {
                t: "people".to_string(),
                id: 9.to_string(),
                relationships: None,
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("firstName".try_into().unwrap(), "Fingle".into());
                    attributes.insert("lastName".try_into().unwrap(), "Dan".into());
                    attributes.insert("twitter".try_into().unwrap(), "fingularity".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/people/9".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
            document::Resource {
                t: "comments".to_string(),
                id: 5.to_string(),
                relationships: {
                    let mut relationships = document::Relationships::new();
                    relationships.insert(
                        "author".try_into().unwrap(),
                        document::Relationship {
                            meta: None,
                            links: None,
                            data: Some(document::Data::Member(Some(document::Identifier {
                                t: "people".to_string(),
                                id: 2.to_string(),
                            }))),
                        },
                    );

                    Some(relationships)
                },
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("body".try_into().unwrap(), "First!".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/comments/5".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
            document::Resource {
                t: "comments".to_string(),
                id: 12.to_string(),
                relationships: {
                    let mut relationships = document::Relationships::new();
                    relationships.insert(
                        "author".try_into().unwrap(),
                        document::Relationship {
                            meta: None,
                            links: None,
                            data: Some(document::Data::Member(Some(document::Identifier {
                                t: "people".to_string(),
                                id: 9.to_string(),
                            }))),
                        },
                    );

                    Some(relationships)
                },
                meta: None,
                attributes: {
                    let mut attributes = document::Attributes::new();
                    attributes.insert("body".try_into().unwrap(), "I like XML better".into());

                    attributes
                },
                links: {
                    let mut links = document::Links::new();
                    links.insert(
                        "self".try_into().unwrap(),
                        document::Link {
                            href: "http://example.com/comments/12".to_string(),
                            meta: None,
                        },
                    );

                    Some(links)
                },
            },
        ])
    };

    let links = {
        let mut links = document::Links::new();
        links.insert(
            "self".try_into().unwrap(),
            document::Link {
                href: "http://example.com/articles/1".to_string(),
                meta: None,
            },
        );

        Some(links)
    };

    let document = Document::Ok {
        data: document::Data::Collection(vec![document::Resource {
            t: "articles".into(),
            id: 1.to_string(),
            attributes,
            relationships,
            links,
            meta: None,
        }]),
        included,
        meta: Default::default(),
        links: Default::default(),
    };

    let expected = helper::load_json("multiple-resource-compound-document.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn errors_example() {
    let document = Document::<document::Resource>::Err {
        meta: Default::default(),
        links: Default::default(),
        errors: vec![
            document::ErrorObject {
                id: None,
                links: None,
                code: None,
                title: None,
                meta: None,
                status: Some(status::StatusCode::FORBIDDEN),
                source: Some(document::Source {
                    pointer: Some("/data/attributes/secretPowers".to_string()),
                    parameter: None,
                }),
                detail: Some("Editing secret powers is not authorized on Sundays.".to_string()),
            },
            document::ErrorObject {
                id: None,
                links: None,
                code: None,
                title: Some("The backend responded with an error".to_string()),
                meta: None,
                status: Some(status::StatusCode::INTERNAL_SERVER_ERROR),
                source: Some(document::Source {
                    pointer: Some("/data/attributes/reputation".to_string()),
                    parameter: None,
                }),
                detail: Some("Reputation service not responding after three requests.".to_string()),
            },
        ],
    };

    let expected = helper::load_json("errors.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn macro_single_resource_document_example() {
    struct Author {
        id: u8,
    }

    jsonapi_resource!(Author; |&self| {
        kind "authors";
        id self.id;
    });

    struct Comment {
        id: u8,
    }

    jsonapi_resource!(Comment; |&self| {
        kind "comments";
        id self.id;
    });

    struct Article {
        id: u8,
        title: String,
        #[allow(dead_code)]
        author: Author,
        #[allow(dead_code)]
        comments: Vec<Comment>,
    }

    jsonapi_resource!(Article; |&self| {
        kind "articles";
        id self.id;

        attrs title;

        has_one author, {
            link "self", format!("http://example.com/articles/{}/relationships/author", self.id);
            link "related", format!("http://example.com/articles/{}/author", self.id);
        }

        has_many comments, {
            link "self", format!("http://example.com/articles/{}/relationships/comments", self.id);
            link "related", format!("http://example.com/articles/{}/comments", self.id);
        }
    });

    let article = Article {
        id: 1,
        title: String::from("JSON:API paints my bike shed!"),
        author: Author { id: 1 },
        comments: vec![Comment { id: 1 }],
    };

    let expected = helper::load_json("single-resource-document.json");
    let actual = serde_json::to_string(&Document::Ok {
        data: document::Data::Member(Some(article.to_resource())),
        meta: Default::default(),
        included: Default::default(),
        links: Default::default(),
    })
    .unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn macro_multiple_resource_document_example() {
    struct Author {
        id: u8,
    }

    jsonapi_resource!(Author; |&self| {
        kind "authors";
        id self.id;
    });

    struct Article {
        id: u8,
        title: String,
        #[allow(dead_code)]
        author: Author,
    }

    jsonapi_resource!(Article; |&self| {
        kind "articles";
        id self.id;

        attrs title;

        has_one author, {
            link "self", format!("http://example.com/articles/{}/relationships/author", self.id);
            link "related", format!("http://example.com/articles/{}/author", self.id);
        }

        link "self", format!("http://example.com/articles/{}", self.id);
    });

    let article1 = Article {
        id: 1,
        title: String::from("JSON:API paints my bike shed!"),
        author: Author { id: 1 },
    };

    let article2 = Article {
        id: 2,
        title: String::from("JSON:API builds my bike shed!"),
        author: Author { id: 1 },
    };

    let expected = helper::load_json("multiple-resource-document.json");
    let actual = serde_json::to_string(&Document::Ok {
        data: document::Data::Collection(vec![article1.to_resource(), article2.to_resource()]),
        meta: Default::default(),
        included: Default::default(),
        links: Default::default(),
    })
    .unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn macro_single_resource_compound_document_example() {
    struct Article {
        id: u8,
        title: &'static str,
        author: Person,
        comments: Vec<Comment>,
    }

    struct Person {
        id: u8,
        first_name: &'static str,
        last_name: &'static str,
        twitter: &'static str,
    }

    struct Comment {
        id: u8,
        body: &'static str,
        author: Person,
    }

    jsonapi_resource!(Article; |&self| {
        kind "articles";
        id self.id;

        attrs title;

        has_one author, {
            data self.author;
            link "self", format!("http://example.com/articles/{}/relationships/author", self.id);
            link "related", format!("http://example.com/articles/{}/author", self.id);
        };
        has_many comments, {
            data self.comments;
            link "self", format!("http://example.com/articles/{}/relationships/comments", self.id);
            link "related", format!("http://example.com/articles/{}/comments", self.id);
        };
    });

    jsonapi_resource!(Person; |&self| {
        kind "people";
        id self.id;

        attrs first_name, last_name, twitter;

        link "self", format!("http://example.com/people/{}", self.id);
    });

    jsonapi_resource!(Comment; |&self| {
        kind "comments";
        id self.id;

        attrs body;

        has_one author;

        link "self", format!("http://example.com/comments/{}", self.id);
    });

    let fingle = Person {
        id: 9,
        first_name: "Fingle",
        last_name: "Dan",
        twitter: "fingularity",
    };

    let comments = vec![
        Comment {
            id: 5,
            body: "First!",
            author: Person {
                id: 2,
                first_name: "",
                last_name: "",
                twitter: "",
            },
        },
        Comment {
            id: 12,
            body: "I like XML better",
            author: Person { ..fingle },
        },
    ];

    let article = Article {
        id: 1,
        title: "JSON:API paints my bike shed!",
        author: Person { ..fingle },
        comments,
    };

    let query = Query::from_params("include=comments,author").unwrap();
    let document = em_jsonapi::document::Builder::new()
        .query(&query)
        .data(&article)
        .build();

    let expected = helper::load_json("single-resource-compound-document.json");
    let actual = serde_json::to_string(&document).unwrap();

    assert_json_eq!(&actual, &expected)
}
