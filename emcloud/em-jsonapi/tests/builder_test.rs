#[macro_use]
extern crate em_jsonapi;

use em_jsonapi::document::{Builder, Data, Document, Link, Links, Meta};
use em_jsonapi::query::params::{Field, Fields};
use em_jsonapi::{Model, Query};
use std::convert::TryInto;

mod helper;

#[derive(Clone)]
struct Association {
    id: u8,
    name: String,
    should_be_ignored: String,
}

jsonapi_resource!(Association; |&self| {
    kind "association";
    id self.id;

    attrs name, should_be_ignored;
});

#[derive(Clone)]
struct TestModel {
    id: u8,
    name: String,
    should_be_ignored: String,
    one: Association,
    ignored: Association,
    many: Vec<Association>,
}

jsonapi_resource!(TestModel; |&self| {
    kind "tests";
    id self.id;

    attrs name, should_be_ignored;

    has_one one, ignored;
    has_many many;
});

#[test]
fn builds_document_with_query() {
    let query = Query {
        includes: Some(
            vec![
                "one".to_string(),
                "many".to_string(),
                "ignored_field_included".to_string(),
            ]
            .into_iter()
            .collect(),
        ),
        fields: Some(Fields(vec![
            Field {
                source: String::from("tests"),
                fields: vec![String::from("name")].into_iter().collect(),
            },
            Field {
                source: String::from("one"),
                fields: vec![String::from("name")].into_iter().collect(),
            },
            Field {
                source: String::from("many"),
                fields: vec![String::from("name")].into_iter().collect(),
            },
        ])),
        sorts: None,
        page: None,
        filter: None,
    };

    let test_has_one = Association {
        id: 2,
        name: String::from("test's has one"),
        should_be_ignored: String::from("should not be in document"),
    };

    let test_ignored = test_has_one.clone();

    let test_has_many = Association {
        id: 1,
        name: String::from("test's has many"),
        should_be_ignored: String::from("should not be in document"),
    };

    let another_test_has_one = Association {
        id: 1,
        name: String::from("another test's has one"),
        should_be_ignored: String::from("should not be in document"),
    };

    let another_test_ignored = another_test_has_one.clone();

    let another_test_has_many = Association {
        id: 2,
        name: String::from("another test's has many"),
        should_be_ignored: String::from("should not be in document"),
    };

    let models = vec![
        TestModel {
            id: 1,
            name: String::from("a test"),
            should_be_ignored: String::from("should not be in document"),
            one: test_has_one.clone(),
            ignored: test_ignored,
            many: vec![test_has_many.clone()],
        },
        TestModel {
            id: 2,
            name: String::from("another test"),
            should_be_ignored: String::from("should not be in document"),
            one: another_test_has_one.clone(),
            ignored: another_test_ignored,
            many: vec![another_test_has_many.clone()],
        },
    ];

    let (test_has_one_resource, _) = test_has_one.to_resource_with_query(&query);
    let (test_has_many_resource, _) = test_has_many.to_resource_with_query(&query);
    let (another_test_has_one_resource, _) = another_test_has_one.to_resource_with_query(&query);
    let (another_test_has_many_resource, _) = another_test_has_many.to_resource_with_query(&query);

    let actual = Builder::new().query(&query).data(&models).build();
    let expected = Document::Ok {
        data: Data::Collection(
            models
                .iter()
                .map(|model| model.to_resource_with_query(&query).0)
                .collect(),
        ),
        meta: Default::default(),
        links: Default::default(),
        included: Some(vec![
            test_has_one_resource,
            test_has_many_resource,
            another_test_has_one_resource,
            another_test_has_many_resource,
        ]),
    };

    let actual = serde_json::to_string(&actual).unwrap();
    let expected = serde_json::to_string(&expected).unwrap();

    assert_json_eq!(&actual, &expected)
}

#[test]
fn builds_without_query() {
    let model = TestModel {
        id: 1,
        name: String::from("a test"),
        should_be_ignored: String::from("actually it should not be ignored for this"),
        one: Association {
            id: 2,
            name: String::from("test's has one"),
            should_be_ignored: String::from("should not be in document"),
        },
        ignored: Association {
            id: 2,
            name: String::from("test's has one"),
            should_be_ignored: String::from("should not be in document"),
        },
        many: vec![Association {
            id: 1,
            name: String::from("test's has many"),
            should_be_ignored: String::from("should not be in document"),
        }],
    };

    let actual = Builder::new()
        .data(&model)
        .meta("test", "this is a test")
        .meta("other", "this is another test")
        .link("related", "/related-stuff")
        .build();

    let expected = Document::Ok {
        data: Data::Member(Some(model.to_resource())),
        meta: {
            let mut meta = Meta::new();
            meta.insert("test".try_into().unwrap(), "this is a test".into());
            meta.insert("other".try_into().unwrap(), "this is another test".into());
            meta
        },
        included: Default::default(),
        links: {
            let mut links = Links::new();
            links.insert("related".try_into().unwrap(), Link::new("/related-stuff"));
            links
        },
    };

    let expected = serde_json::to_string(&expected).unwrap();
    let actual = serde_json::to_string(&actual).unwrap();

    assert_json_eq!(&actual, &expected)
}
