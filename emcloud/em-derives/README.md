# emCloud - derives

A collection of various derive macros designed for emCloud use cases.

## Available Macros

### `FromEnv`

Extracts environment variables matching field names for a struct.  
An attribute may be set to specify a prefix for environment variable names.

**EXAMPLE**

```sh
export CONFIG_GREETING="hello"
export CONFIG_WHO="world"
```

```rust
#[derive(FromEnv)]
#[env(prefix = "CONFIG")]
struct MyConfig {
    greeting: String
    who: String
}

fn main() {
    let config = MyConfig::from_env();
    let greeting = format!("{} {}", config.greeting, config.who);
    assert_eq!(greeting, "hello world".to_string());
}
```
