use proc_macro::TokenStream;
use quote::quote;
use syn::punctuated::Punctuated;

const OPTION_IDENTIFIER: &str = "env";

macro_rules! abort {
    ($e:expr) => {{
        panic!("failed to derive FromEnv : {:#?}", $e);
    }};
}

enum DeriveOptions {
    Default(syn::Lit),
    Prefix(String),
    Nested,
    None,
}

impl DeriveOptions {
    fn from_attribute(attribute: Option<&syn::Attribute>) -> Result<Self, syn::Error> {
        if let Some(attribute) = attribute {
            let token = attribute.parse_args::<syn::Meta>()?;

            match token {
                syn::Meta::List(token) => {
                    abort!(format!("unexpected meta list : {:#?}", token))
                }
                syn::Meta::Path(_) => return Ok(Self::Nested),
                syn::Meta::NameValue(token) => {
                    let id = token.path.get_ident();

                    if let Some(id) = id {
                        return match id.to_string().as_str() {
                            "prefix" => match token.lit {
                                syn::Lit::Str(lit) => Ok(Self::Prefix(lit.value())),
                                _ => abort!("invalid type literal for prefix option"),
                            },
                            "default" => Ok(Self::Default(token.lit)),
                            "nested" => Ok(Self::Nested),
                            _ => Ok(Self::None),
                        };
                    }
                }
            };
        }

        Ok(Self::None)
    }
}

pub(crate) fn parse_and_impl(item: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(item).unwrap_or_else(|e| abort!(e));

    match ast.data {
        syn::Data::Struct(ref ds) => match ds.fields {
            syn::Fields::Named(ref fields) => impl_from_env(&ast, &fields.named),
            _ => panic!("FormEnv must derive a Struct with named fields"),
        },
        _ => panic!("FromEnv must derive a Struct"),
    }
}

fn impl_from_env(
    ast: &syn::DeriveInput,
    fields: &Punctuated<syn::Field, syn::token::Comma>,
) -> TokenStream {
    let struct_name = &ast.ident;

    let prefix = extract_prefix(ast)
        .unwrap_or_else(|e| abort!(e))
        .map_or_else(String::new, |prefix| format!("{}_", prefix));

    let field_values = fields.iter().map(|field| {
        let ident = &field.ident;
        let ty = &field.ty;
        let name = format!(
            "{}{}",
            prefix,
            field.clone().ident.unwrap().to_string().to_uppercase()
        );

        let options = find_option(&field.attrs).unwrap_or_else(|e| abort!(e));

        match options {
            DeriveOptions::Prefix(_) => {
                panic!("#[env(prefix = ...)] may only be used on a Struct.")
            }
            DeriveOptions::None => quote! {
                #ident: Self::get_var::<#ty>(#name)?
            },
            DeriveOptions::Nested => quote! {
                #ident: #ty::from_env()?
            },
            DeriveOptions::Default(default) => quote! {
                #ident: Self::get_var::<#ty>(#name).unwrap_or(#default.into())
            },
        }
    });

    let tokens = quote! {
        impl crate::traits::FromEnv for #struct_name {
            fn from_env() -> Result<Self, crate::traits::from_env::Error> {
                Ok(
                    Self {
                        #(#field_values,)*
                    }
                )
            }
        }
    };

    tokens.into()
}

fn extract_prefix(ast: &syn::DeriveInput) -> Result<Option<String>, syn::Error> {
    match find_option(&ast.attrs)? {
        DeriveOptions::Prefix(prefix) => Ok(Some(prefix)),
        _ => Ok(None),
    }
}

fn find_option(attrs: &[syn::Attribute]) -> Result<DeriveOptions, syn::Error> {
    let option = attrs.iter().find(|a| {
        let path = &a.path;
        let name = quote!(#path).to_string();
        name == OPTION_IDENTIFIER
    });

    DeriveOptions::from_attribute(option)
}
