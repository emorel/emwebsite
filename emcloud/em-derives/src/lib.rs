#![deny(clippy::pedantic)]

extern crate proc_macro;
use proc_macro::TokenStream;

mod from_env;

/// Implements a `from_env()` function which constructs the item based on
/// environment variables.
///
/// This macro also casts any values to the type given to the struct field.
///
/// A struct attribute `env(prefix)` may also be passed which appends the
/// given prefix to the variable name.
///
/// # Examples
///
/// - Without a prefix
///
/// ```rust
/// std::env::set_var("A_STRING", "Hello world !");
/// std::env::set_var("AN_INT", "42");
///
/// #[derive(derives::FromEnv)]
/// struct MyConfig {
///     a_string: String,
///     an_int: u8
/// }
///
/// let config = MyConfig::from_env();
/// assert_eq!(config.a_string, "Hello world !".to_string());
/// assert_eq!(config.an_int, 42);
/// ```
///
/// - With a prefix
///
/// ```rust
/// std::env::set_var("CONFIG_A_STRING", "Hello world !");
/// std::env::set_var("CONFIG_AN_INT", "42");
///
/// #[derive(derives::FromEnv)]
/// #[env(prefix = "CONFIG")]
/// struct MyConfig {
///     a_string: String,
///     an_int: u8
/// }
///
/// let config = MyConfig::from_env();
/// assert_eq!(config.a_string, "Hello world !".to_string());
/// assert_eq!(config.an_int, 42);
/// ```
///
/// - With nested configuration structs
///
/// ```rust
/// std::env::set_var("HELLO_TYPE", "Hello");
/// std::env::set_var("HELLO_TARGET", "world");
///
/// #[derive(derives::FromEnv)]
/// struct SayHelloConfig {
///     hello_type: String,
///
///     #[env(nested)]
///     to_whom: WorldConfig
/// }
///
/// #[derive(derives::FromEnv)]
/// struct WorldConfig {
///     hello_target: String
/// }
///
/// let config = SayHelloConfig::from_env();
/// let greeting = format!("{} {} !", config.hello_type, config.to_whom.hello_target);
///
/// assert_eq!(greeting, "Hello world !".to_string());
/// ```
#[proc_macro_derive(FromEnv, attributes(env))]
pub fn derive_from_env(item: TokenStream) -> TokenStream {
    from_env::parse_and_impl(item)
}
