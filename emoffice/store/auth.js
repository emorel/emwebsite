import Vue from 'vue'
import { utils } from 'jsonapi-vuex'

export const state = () => ({
  /**
   * Whether the user is authenticated.
   *
   * An `null` value means that the state of the session is unknown, e.g. it has
   * not been initialized yet.
   */
  authenticated: null,
  /**
   * Whether the cookie session has been initialized.
   */
  initialized: false,
  /**
   * The information of the authenticated user.
   */
  user: null,
  /**
   * ID of the refresh timeout function, used to clear the timeout when logging out.
   */
  _refreshTimer: null,
})

export const mutations = {
  SET(state, { key, value }) {
    Vue.set(state, key, value)
  },

  RESET(state) {
    state.authenticated = false
    state.user = null
    state.expiry = null

    if (state._refreshTimer) {
      clearTimeout(state._refreshTimer)
      state._refreshTimer = null
    }
  },
}

export const actions = {
  /**
   * Dispatches a request to the API to log in.
   *
   * If authentication is successful, it follows the related link of the
   * response to fetch the user's information to persist into the store.
   *
   * @param {String} payload The information to log in with.
   * @param {String} payload.email The email address to log in with.
   * @param {String} payload.password The password to log in with.
   */
  async login({ commit, dispatch }, { email, password }) {
    const loginResponse = await this.$axios.$post('/user/auth', {
      email,
      password,
    })

    const userResponse = await this.$axios.$get(loginResponse.links.related)
    const user = utils.jsonapiToNorm(userResponse.data)

    commit('SET', { key: 'user', value: user })
    commit('SET', { key: 'authenticated', value: true })

    setAutoRefreshTimeout({ dispatch, commit }, loginResponse.meta.expiry)
  },

  /**
   * Dispatches a request to the API to log out.
   *
   * Essentially this instructs the API to purge the session cookie.
   */
  async logout({ commit }) {
    await this.$axios.$delete('/user/auth')
    commit('RESET')
  },

  /**
   * Sends a request to check if a session is valid.
   *
   * Used primarily on the application's first load.
   */
  async init({ commit, dispatch }) {
    try {
      const checkResponse = await this.$axios.$patch('/user/auth')
      const userResponse = await this.$axios.$get(checkResponse.links.related)
      const user = utils.jsonapiToNorm(userResponse.data)

      commit('SET', { key: 'user', value: user })
      commit('SET', { key: 'initialized', value: true })
      commit('SET', { key: 'authenticated', value: true })

      setAutoRefreshTimeout({ dispatch, commit }, checkResponse.meta.expiry)
    } catch (e) {
      commit('SET', { key: 'user', value: null })
      commit('SET', { key: 'initialized', value: true })
      commit('SET', { key: 'authenticated', value: false })
    }
  },

  /**
   * Sends a request to the API to refresh the session's duration.
   */
  async refresh({ commit, dispatch }) {
    try {
      const response = await this.$axios.$patch('/user/auth')
      setAutoRefreshTimeout({ dispatch, commit }, response.meta.expiry)
    } catch (e) {
      commit('RESET')
    }
  },
}

/**
 * Sets a timeout to automatically dispatch a request to refresh the session's duration.
 *
 * @param {Object} store Vuex store context
 * @param {Function} store.dispatch Vuex `dispatch` function
 * @param {Function} store.commit Vuex `commit` function
 * @param {Number} expiry When the current session will expire in seconds
 */
function setAutoRefreshTimeout({ dispatch, commit }, expiry) {
  // Refresh automatically every expiry - 1 minute
  const timer = setTimeout(() => {
    dispatch('refresh')
  }, expiry * 1000 - 60000)

  commit('SET', { key: '_refreshTimer', value: timer })
}
