import { Component, Vue } from 'vue-property-decorator'
import { dig } from '@/utils/object'

export function createMapper(store, keys) {
  const computed = {}
  const data = {
    FormMapper__store: store,
    FormMapper__storeKeys: {},
    FormMapper__keys: [],
  }

  for (const key of keys) {
    const localKey = `FormMapper__${key}`

    data.FormMapper__keys.push(localKey)
    data.FormMapper__storeKeys[localKey] = key
    data[localKey] = null

    computed[key] = {
      get() {
        return this[localKey]
      },

      set(v) {
        this[localKey] = v
      },
    }
  }

  function formData() {
    return Object.fromEntries(
      Object.values(this.FormMapper__storeKeys).map((key) => {
        return [key, this[key]]
      })
    )
  }

  function mapFields() {
    const state = dig(this.$store.state, this.FormMapper__store.split('/'))

    for (const key of this.FormMapper__keys) {
      const stateKey = this.FormMapper__storeKeys[key]
      const stateValue = state[stateKey]
      this[key] = stateValue
    }
  }

  function commitForm() {
    this.$store.commit(
      `${this.FormMapper__store}/updateFields`,
      this.formData()
    )
  }

  @Component({
    data: () => data,
    computed,
    methods: {
      mapFields,
      formData,
      commitForm,
    },

    created() {
      this.mapFields()
    },
  })
  class FormMapper extends Vue {}

  return FormMapper
}
