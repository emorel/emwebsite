import { POSITION } from 'vue-toastification'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  server: {
    host: process.env.OFFICE_HOST,
    port: process.env.OFFICE_PORT,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: (titleChunk) => {
      return titleChunk ? `emOffice - ${titleChunk}` : 'emOffice'
    },
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    noscript: [
      {
        innerHTML: 'Please enable Javascript to use this website',
      },
    ],
    __dangerouslyDisableSanitizers: ['noscript'],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/styles/main'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/errors', '@/plugins/jsonapi-vuex', '@/plugins/auth'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'vue-toastification/nuxt',
  ],

  router: {
    middleware: ['auth'],
  },

  toast: {
    position: POSITION.TOP_RIGHT,
    timeout: 2500,
    cssFile: '@/assets/styles/vendors/toast.scss',
    maxToasts: 5,
    closeOnClick: false,
    newestOnTop: true,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.OFFICE_AXIOS_URL,
    debug: process.env.OFFICE_AXIOS_DEBUG,
    progress: false,
    credentials: true,
    headers: {
      common: {
        Accept: 'application/vnd.api+json',
      },

      get: {},
      post: {
        'Content-Type': 'application/vnd.api+json',
      },
      put: {
        'Content-Type': 'application/vnd.api+json',
      },
      delete: {},
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
