module.exports = {
  testMatch: ['**/tests/unit/**/?(*.)+(spec|test).[jt]s?(x)'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
  },
  moduleFileExtensions: ['ts', 'js', 'vue', 'json'],
  transform: {
    '^.+\\.ts$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
  },
  setupFilesAfterEnv: ['<rootDir>/tests/unit/support/setup.js'],
  collectCoverage: true,
  collectCoverageFrom: [
    '<rootDir>/utils/**/*',
    '<rootDir>/mixins/**/*',
    '<rootDir>/components/**/*',
    '<rootDir>/pages/**/*',
  ],
}
