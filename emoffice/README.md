# emOffice

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Resources

[Icons](https://heroicons.com/)  
[Tailwind documentation](https://tailwindcss.com/docs)

## FAQ

Q: I would like to pass a prop to an icon component in a toast.  
A: `render: (el) => el(Icon, { props: { ... } })`

Q: I have the following error when starting up the service `[nodemon] Internal watch failed: ENOSPC: System limit for number of file watchers reached, watch '...'`.  
A: On the host : `sudo sysctl fs.inotify.max_user_watches=582222 && sudo sysctl -p`.
