#!/bin/sh
set -e

yarn install --check-files

exec "$@"
