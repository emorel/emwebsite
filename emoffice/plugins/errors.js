import JsonApiErrors from '@/components/JsonApi/Errors'

export const ERR_SERVER_UNAVAILABLE = 'The server is unavailable'
export const ERR_NETWORK_OFFLINE = 'Your browser is offline'
export const ERR_UNKNOWN = 'An unknown error has occurred'

/**
 * This plugin attempts to parse and display errors.
 *
 * The handler expects an error object or an array of error objects in JSON:API format.
 * Any error object that is not in JSON:API format is ignored and a general
 * error message is displayed to the user instead.
 */
export default function (context, inject) {
  inject('error', (error) => {
    if (error.isAxiosError) {
      handleAxiosError(context, error)
      return
    }

    if (error.message) {
      context.$toast.error(error.message)
    } else {
      unknownError(context)
    }
  })
}

function handleAxiosError(context, error) {
  // the error is an API response
  if (error.response) {
    const { errors } = error.response.data

    return context.$toast.error({
      component: JsonApiErrors,
      props: { errors },
    })
  }

  // check for common cases for Axios errors
  if (!navigator.onLine) {
    return context.$toast.error(ERR_NETWORK_OFFLINE)
  } else if (checkApiUnreachable(error)) {
    return context.$toast.error(ERR_SERVER_UNAVAILABLE)
  }

  // display unknown error
  unknownError(context)
}

function checkApiUnreachable(error) {
  const errorMessage = error.message ? error.message : ''

  return errorMessage.match(/network error/gi)
}

function unknownError(context) {
  context.$toast.error(ERR_UNKNOWN)
}
