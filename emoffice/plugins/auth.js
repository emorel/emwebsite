/**
 * Listens globally to changes to `$store.state.auth.authenticated`.
 *
 * Redirects to `/login` when it changes to false.
 * Redirects to `/` when it changes to true.
 *
 * On initial load, `previousState` is `null`, the watcher will check if already
 * on the relevant page, so as to avoid redirecting to the same page.
 */
export default function ({ store, route, redirect }, _) {
  const initialPath = route.fullPath

  store.watch(
    ({ auth }) => auth.authenticated,
    (authenticated, previousState) => {
      if (authenticated) {
        // if initial load and not on login page, no need to redirect
        if (previousState === null && initialPath !== '/login') {
          return
        }

        redirect('/')
      } else {
        // if initial load and on login page, no need to redirect
        if (previousState === null && initialPath === '/login') {
          return
        }

        redirect('/login')
      }
    }
  )
}
