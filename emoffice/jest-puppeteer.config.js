module.exports = {
  preset: 'jest-puppeteer',
  launch: {
    headless: process.env.HEADLESS || process.env.CI || false,
    product: 'firefox',
    defaultViewport: {
      height: 600,
      width: 800,
    },
  },
}
