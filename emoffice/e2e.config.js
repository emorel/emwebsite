module.exports = {
  verbose: true,
  bail: true,
  preset: 'jest-puppeteer',
  // doubled as e2e tests are slow in general
  testTimeout: 10000,
  testMatch: ['**/tests/e2e/**/?(*.)+(spec|test).[jt]s?(x)'],
  globalSetup: '<rootDir>/tests/e2e/support/setup.js',
  globalTeardown: '<rootDir>/tests/e2e/support/teardown.js',
  moduleFileExtensions: ['ts', 'js', 'vue', 'json'],
  transform: {
    '^.+\\.ts$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/(?!jsonapi-vuex)'],
  // TODO: Fix e2e coverage if at all possible, may need to look at puppeteer-to-istanbul
  // collectCoverage: true,
  // collectCoverageFrom: [
  //   '<rootDir>/utils/**/*',
  //   '<rootDir>/mixins/**/*',
  //   '<rootDir>/components/**/*',
  //   '<rootDir>/pages/**/*',
  // ],
}
