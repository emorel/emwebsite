/**
 * This middleware checks if the session has been initialized.
 *
 * TODO
 *  This could be done in the auth plugin instead, but it shows the login page if
 *  session is already valid before redirecting in that case.
 */
export default async ({ store }) => {
  const auth = store.state.auth

  if (!auth.initialized) {
    await store.dispatch('auth/init')
  }
}
