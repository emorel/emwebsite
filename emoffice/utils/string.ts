/**
 * Capitalizes the given string *str*.
 *
 * @param str The string to be capitalized.
 * @returns the original string capitalize.
 */
export function capitalize(str: string): string {
  return `${str.charAt(0).toUpperCase()}${str.slice(1)}`
}
