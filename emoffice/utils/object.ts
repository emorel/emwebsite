export function dig(object: { [key: string]: any }, keys: string[]) {
  let obj: Object = object

  for (const key of keys) {
    // @ts-ignore
    obj = obj[key]
  }

  return obj
}
