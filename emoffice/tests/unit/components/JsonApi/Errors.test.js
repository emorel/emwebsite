import { mount } from '@vue/test-utils'
import Errors from '@/components/JsonApi/Errors'

function createErrors(num, key = 'detail') {
  const errors = []
  for (let i = 0; i < num; i++) {
    errors.push({ [key]: `Error ${i + 1}` })
  }

  return errors
}

describe('Errors', () => {
  it('shows one error as a <div>', async () => {
    const wrapper = mount(Errors, {
      propsData: { errors: createErrors(1) },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('shows multiple errors as a <ul>', async () => {
    const wrapper = mount(Errors, { propsData: { errors: createErrors(2) } })
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('shows as many errors as the limit and reveal them when clicking the <a> element', async () => {
    const wrapper = mount(Errors, {
      propsData: { errors: createErrors(5), limit: 3 },
    })
    await wrapper.vm.$nextTick()

    const revealAllButton = wrapper.find('a')

    expect(wrapper.findAll('li:not([style*="display: none;"])').length).toEqual(
      3
    )
    expect(wrapper.findAll('li[style*="display: none;"]').length).toEqual(2)
    expect(revealAllButton.text()).toEqual('Show all 5 errors')
    expect(wrapper.html()).toMatchSnapshot()

    revealAllButton.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.findAll('li:not([style*="display: none;"])').length).toEqual(
      5
    )
    expect(wrapper.findAll('li[style*="display: none;"]').length).toEqual(0)
    expect(revealAllButton.text()).toEqual('Show less')
    expect(wrapper.html()).toMatchSnapshot()
  })

  it('ignores duplicates', async () => {
    // on detail
    let errors = createErrors(2)
    errors = [...errors, ...errors]
    let wrapper = mount(Errors, { propsData: { errors } })
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).toMatchSnapshot()

    // on title
    errors = createErrors(2, 'title')
    errors = [...errors, ...errors]
    wrapper = mount(Errors, { propsData: { errors } })
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).toMatchSnapshot()

    // on code
    errors = createErrors(2)
    errors = errors.map((error, index) => {
      error.detail = 'same message, different code'
      error.code = index + 1
      return error
    })
    errors = [...errors, ...errors]
    wrapper = mount(Errors, { propsData: { errors } })
    await wrapper.vm.$nextTick()
    expect(wrapper.html()).toMatchSnapshot()
  })
})
