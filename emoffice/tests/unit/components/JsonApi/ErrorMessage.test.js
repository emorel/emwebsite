import { mount } from '@vue/test-utils'
import ErrorMessage from '@/components/JsonApi/ErrorMessage'

describe('ErrorMessage', () => {
  it('displays a formatted error message', () => {
    // only detail
    let wrapper = mount(ErrorMessage, {
      context: { props: { error: { detail: 'Hello world' } } },
    })
    expect(wrapper.html()).toMatchSnapshot()

    // only title
    wrapper = mount(ErrorMessage, {
      context: { props: { error: { title: 'Goodbye world' } } },
    })
    expect(wrapper.html()).toMatchSnapshot()

    // title + detail
    wrapper = mount(ErrorMessage, {
      context: {
        props: { error: { detail: 'Hello world', title: 'Goodbye World' } },
      },
    })
    expect(wrapper.html()).toMatchSnapshot()

    // title + code
    wrapper = mount(ErrorMessage, {
      context: { props: { error: { title: 'Goodbye world', code: '123' } } },
    })
    expect(wrapper.html()).toMatchSnapshot()

    // detail + code
    wrapper = mount(ErrorMessage, {
      context: { props: { error: { detail: 'Hello world', code: '321' } } },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
