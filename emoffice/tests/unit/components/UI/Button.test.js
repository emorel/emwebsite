import { mount } from '@vue/test-utils'
import Button from '@/components/UI/Button'

describe('Button', () => {
  const wrapper = mount(Button)

  it('hides spinner when `loading` is false', async () => {
    wrapper.setProps({ loading: false })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('span').isVisible()).toBe(false)
  })

  it('hides spinner when `loading` is true', async () => {
    wrapper.setProps({ loading: true })
    await wrapper.vm.$nextTick()
    expect(wrapper.find('span').isVisible()).toBe(true)
  })
})
