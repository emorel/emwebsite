import { mount } from '@vue/test-utils'
import Subtitle from '@/components/UI/Subtitle'

describe('Subtitle', () => {
  const wrapper = mount(Subtitle, {
    slots: { default: 'Test Subtitle' },
    context: { props: { size: 3 } },
  })

  it('display the title with the specified size', () => {
    expect(wrapper.find('h3').classes()).toContain('text-3xl')
    expect(wrapper.text()).toEqual('Test Subtitle')
  })
})
