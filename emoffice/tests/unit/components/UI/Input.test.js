import { mount } from '@vue/test-utils'
import Input from '@/components/UI/Input'

describe('Input', () => {
  test.each([
    'button',
    'checkbox',
    'color',
    'date',
    'datetime-local',
    'email',
    'file',
    'hidden',
    'image',
    'month',
    'number',
    'password',
    'radio',
    'range',
    'reset',
    'search',
    'submit',
    'tel',
    'text',
    'time',
    'url',
    'week',
  ])('renders %s inputs', (type) => {
    const wrapper = mount(Input, { attrs: { type } })
    expect(wrapper.element).toMatchSnapshot()
  })
})
