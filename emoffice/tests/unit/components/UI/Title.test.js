import { mount } from '@vue/test-utils'
import Title from '@/components/UI/Title'

describe('Title', () => {
  test('display the title with the specified size', () => {
    const wrapper = mount(Title, {
      slots: { default: 'Test Title' },
      context: { props: { size: 3 } },
    })

    expect(wrapper.find('h1').classes()).toContain('text-3xl')
    expect(wrapper.text()).toEqual('Test Title')
  })
})
