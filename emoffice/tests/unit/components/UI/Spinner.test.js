import { mount } from '@vue/test-utils'
import Spinner from '@/components/UI/Spinner'

describe('Spinner', () => {
  const wrapper = mount(Spinner)

  it('renders the spinner', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
})
