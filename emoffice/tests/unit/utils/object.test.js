import { dig } from '@/utils/object'

describe('dig()', () => {
  const testObject = { test: { nested: 'value' } }

  it('returns undefined when given keys do not exist', () => {
    expect(dig(testObject, ['test', 'wrongKey'])).toBeUndefined()
  })

  it('returns the value when given keys exist', () => {
    expect(dig(testObject, ['test', 'nested'])).toEqual('value')
  })
})
