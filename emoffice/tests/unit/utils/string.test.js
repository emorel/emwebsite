import { capitalize } from '@/utils/string'

describe('capitalize()', () => {
  test('"hello world" => "Hello world"', () => {
    expect(capitalize('hello world')).toEqual('Hello world')
  })

  test('"_hello World" => "_hello World"', () => {
    expect(capitalize('_hello World')).toEqual('_hello World')
  })
})
