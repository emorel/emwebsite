import { mount } from '@vue/test-utils'
import LoginPage from '@/pages/login.vue'

describe('Login Page', () => {
  const wrapper = mount(LoginPage)

  it('renders the page', async () => {
    await wrapper.vm.$nextTick()
    expect(wrapper.element).toMatchSnapshot()
  })
})
