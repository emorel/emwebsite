import NotFoundPage from '@/pages/*.vue'

describe('page not found', () => {
  const context = {
    redirect: jest.fn(),
    $toast: { error: jest.fn() },
  }

  it('redirects to previous page', async () => {
    context.from = { name: '/login', fullPath: '/login' }
    context.route = { name: '/definitely-does-not-exist' }

    await NotFoundPage.fetch(context)

    expect(context.redirect).toHaveBeenCalledWith(404, '/login')
    expect(context.$toast.error).toHaveBeenCalledWith(
      'This page does not exist.'
    )
  })

  it('redirects to / when previous page is not available', async () => {
    context.route = { name: '/definitely-does-not-exist' }
    context.from = { ...context.route }

    await NotFoundPage.fetch(context)

    expect(context.redirect).toHaveBeenCalledWith(404, '/')
    expect(context.$toast.error).toHaveBeenCalledWith(
      'This page does not exist.'
    )
  })
})
