export async function expectToast(text, opts = {}) {
  const { waitForDismissal } = opts

  await page.waitForSelector('.Vue-Toastification__toast')
  const toast = await page.$('.Vue-Toastification__toast')
  await expect(toast).toMatch(text)

  if (waitForDismissal) {
    await page.waitForSelector('.Vue-Toastification__toast', { hidden: true })
  }
}
