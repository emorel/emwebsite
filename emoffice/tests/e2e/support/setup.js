/* eslint-disable no-console */
const fs = require('fs')
const util = require('util')
const _exec = require('child_process').exec
const exec = util.promisify(_exec)

const info = console.info
const FIXTURE_DIR = './tests/e2e/fixtures'

/**
 * Loads all fixtures in e2e/fixtures folder.
 */
async function loadFixtures() {
  info('=> Fixtures')

  const fixtures = Object.fromEntries(
    fs
      .readdirSync(FIXTURE_DIR)
      .map((filename) => [
        filename,
        fs.readFileSync(`${FIXTURE_DIR}/${filename}`).toString(),
      ])
  )

  info(`  -> Found ${Object.keys(fixtures).length} fixtures`)

  const { Client } = require('pg')
  const client = new Client({
    host: 'localhost',
    port: 5432,
    database: 'emcloud_e2e',
    user: 'em',
    password: 'pass',
  })

  await client.connect()

  for (const key in fixtures) {
    const sql = fixtures[key]
    info(`  -> Executing ${key}`)
    await client.query(sql)
  }

  await client.end()
}

/**
 * Initializes the Nuxt application.
 *
 * This also builds the application if not in a CI environment.
 * For CI environments, the project must be built manually first, this allows to
 * reuse any previous build job which is very common in CI.
 *
 * @returns a nuxt instance
 */
async function initEmOffice() {
  const { loadNuxt } = require('nuxt')
  const wasBuilt = fs.existsSync('./dist') && fs.existsSync('.nuxt/dist/server')

  info('=> Nuxt')

  if (process.env.CI) {
    info('  -> CI Environment, skip building')
  } else if (wasBuilt) {
    info('  -> Found existing nuxt build, skip building')
  } else if (process.env.NO_AUTO_BUILD) {
    info('  -> NO_AUTO_BUILD was set, skip building')
  } else {
    info('  -> Building app, this will take a while...')
    info(
      '    Note : if a build is already present, this step is automatically skipped.\n',
      '    Note : you can force-skip this step by setting the NO_AUTO_BUILD env variable'
    )

    await exec('yarn build')
  }

  info('  -> Loading app')
  const nuxt = await loadNuxt('start')
  info(`  -> App listening on localhost:8080`)
  nuxt.listen(8080, 'localhost')
  await nuxt.ready()

  return nuxt
}

module.exports = async function () {
  info('\n\n=== SETUP ===\n')

  await loadFixtures()

  const nuxt = await initEmOffice()
  global.__NUXT__ = nuxt

  await require('jest-environment-puppeteer/setup')()

  info('\n')
}
