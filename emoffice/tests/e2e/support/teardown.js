/* eslint-disable no-console */
module.exports = async function () {
  console.info('\n=== TEARDOWN ===\n')

  if (global.__NUXT__) {
    console.info('=> Exiting application')
    await global.__NUXT__.close()
    await require('jest-environment-puppeteer/teardown')()
  }

  // if not in ci, clean the database
  // we do not need to do this in ci as the service is entirely dropped
  //
  // easiest way to clean everything is to just drop the database and recreate
  // it, when the emcloud container is restarted, diesel will re-execute the
  // migrations as if nothing happened
  if (!process.env.CI) {
    console.info('=> Dropping database')
    const { Client } = require('pg')
    const client = new Client({
      host: 'localhost',
      port: 5432,
      database: 'postgres',
      user: 'em',
      password: 'pass',
    })
    await client.connect()
    await client.query('DROP DATABASE emcloud_e2e WITH (FORCE); ')
    await client.query('CREATE DATABASE emcloud_e2e;')
    await client.end()
  }

  console.info('\n')
}
