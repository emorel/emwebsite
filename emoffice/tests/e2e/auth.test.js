import { expectToast } from './support/shared_examples'

describe('Auth', () => {
  it('shows toast when missing required fields', async () => {
    await page.goto('http://localhost:8080/login')
    await expect(page).toFillForm('form', {
      email: '',
      password: 'hunter2',
    })
    await expect(page).toClick('button[type="submit"]', { text: 'log in' })
    await expectToast('An email and password are required')

    await page.reload()

    await expect(page).toFillForm('form', {
      email: 'test@example.com',
      password: '',
    })
    await expect(page).toClick('button[type="submit"]', { text: 'log in' })
    await expectToast('An email and password are required')
  })

  it('shows toast when email is invalid', async () => {
    await page.goto('http://localhost:8080/login')
    await expect(page).toFillForm('form', {
      email: 'bad-email@example.com',
      password: 'hunter2',
    })
    await expect(page).toClick('button[type="submit"]', { text: 'log in' })
    await expectToast('Record was not found')
  })

  it('shows toast when password is invalid', async () => {
    await page.goto('http://localhost:8080/login')
    await expect(page).toFillForm('form', {
      email: 'test@example.com',
      password: 'hunter3',
    })
    await expect(page).toClick('button[type="submit"]', { text: 'log in' })
    await expectToast('Invalid password')
  })

  it('can log in and log out', async () => {
    await page.goto('http://localhost:8080/login')

    // logging in
    await expect(page).toFillForm('form', {
      email: 'test@example.com',
      password: 'hunter2',
    })
    await expect(page).toClick('button[type="submit"]', { text: 'log in' })
    await page.waitForTimeout(1500)
    let title = await page.title()
    await expect(title).toMatch('Dashboard')

    // logging out
    await expect(page).toClick('button[title="Log out"]')
    await page.waitForTimeout(1500)
    title = await page.title()
    await expect(title).toMatch('Log In')
  })
})
