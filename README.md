# emWebsite

![Pipeline](https://gitlab.com/emorel/emwebsite/badges/main/pipeline.svg?style=flat-square&key_text=Pipeline&ignore_skipped=true) ![emCloud Coverage](https://gitlab.com/emorel/emwebsite/badges/main/coverage.svg?style=flat-square&job=emcloud:test:coverage&key_text=emCloud%20Coverage&key_width=125) ![emOffice Coverage](https://gitlab.com/emorel/emwebsite/badges/main/coverage.svg?style=flat-square&job=emoffice:test:unit&key_text=emOffice%20Coverage&key_width=125)

My personal website, portfolio and cloud.

The end goal of this project is to replace my dependence on the Google ecosystem
for most of my daily usage.

## Installation

```sh
git clone git@gitlab.com:emorel/emwebsite
cd emwebsite
bin/dev
```

## Available scripts

Inside the `bin` folder, can be found some Bash scripts to manage the various containers more easily.

### `bin/dev`

Fires up a dev environment to start working right away. All regular `docker-compose up` arguments are supported.

### `bin/psql`

Starts a PSQL session in the `pg` container as `em`.

### `bin/diesel`

Exposes the Diesel CLI in the `cloud` container context. Not to be confused with
Vin Diesel.

### `bin/build`

Builds the `llvm-tools` image and pushes it to Gitlab.

## Note about Rust Analyzer

Rust Analyzer runs `cargo check` behind the scenes, which by default will build your project inside as a `debug` build. Since this project runs inside Docker, the Make command that watches for file change also runs `cargo build` to update the application.

This unfortunately means that every file change will result in _two_ full debug compilations as both `cargo check` and `cargo build` will invalidate each other's caches, regardless of the build target.

In order to prevent invalidation, you must change Rust Analyzer's target directory to a different directory from the debug target - e.g. `./target/check`. Although note that it will obviously duplicate the same build, taking twice as much space.

If you are using VSCode, this setting is already set in the workspace settings.
